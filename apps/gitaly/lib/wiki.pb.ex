defmodule Gitaly.WikiGetAllPagesRequest.SortBy do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :TITLE | :CREATED_AT

  field(:TITLE, 0)
  field(:CREATED_AT, 1)
end

defmodule Gitaly.WikiListPagesRequest.SortBy do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :TITLE | :CREATED_AT

  field(:TITLE, 0)
  field(:CREATED_AT, 1)
end

defmodule Gitaly.WikiCommitDetails do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          email: binary,
          message: binary,
          user_id: integer,
          user_name: binary
        }

  defstruct name: "",
            email: "",
            message: "",
            user_id: 0,
            user_name: ""

  field(:name, 1, type: :bytes)
  field(:email, 2, type: :bytes)
  field(:message, 3, type: :bytes)
  field(:user_id, 4, type: :int32, json_name: "userId")
  field(:user_name, 5, type: :bytes, json_name: "userName")
end

defmodule Gitaly.WikiPageVersion do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit: Gitaly.GitCommit.t() | nil,
          format: String.t()
        }

  defstruct commit: nil,
            format: ""

  field(:commit, 1, type: Gitaly.GitCommit)
  field(:format, 2, type: :string)
end

defmodule Gitaly.WikiPage do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          version: Gitaly.WikiPageVersion.t() | nil,
          format: String.t(),
          title: binary,
          url_path: String.t(),
          path: binary,
          name: binary,
          historical: boolean,
          raw_data: binary
        }

  defstruct version: nil,
            format: "",
            title: "",
            url_path: "",
            path: "",
            name: "",
            historical: false,
            raw_data: ""

  field(:version, 1, type: Gitaly.WikiPageVersion)
  field(:format, 2, type: :string)
  field(:title, 3, type: :bytes)
  field(:url_path, 4, type: :string, json_name: "urlPath")
  field(:path, 5, type: :bytes)
  field(:name, 6, type: :bytes)
  field(:historical, 7, type: :bool)
  field(:raw_data, 8, type: :bytes, json_name: "rawData")
end

defmodule Gitaly.WikiWritePageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          name: binary,
          format: String.t(),
          commit_details: Gitaly.WikiCommitDetails.t() | nil,
          content: binary
        }

  defstruct repository: nil,
            name: "",
            format: "",
            commit_details: nil,
            content: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:name, 2, type: :bytes)
  field(:format, 3, type: :string)
  field(:commit_details, 4, type: Gitaly.WikiCommitDetails, json_name: "commitDetails")
  field(:content, 5, type: :bytes)
end

defmodule Gitaly.WikiWritePageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          duplicate_error: binary
        }

  defstruct duplicate_error: ""

  field(:duplicate_error, 1, type: :bytes, json_name: "duplicateError")
end

defmodule Gitaly.WikiUpdatePageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          page_path: binary,
          title: binary,
          format: String.t(),
          commit_details: Gitaly.WikiCommitDetails.t() | nil,
          content: binary
        }

  defstruct repository: nil,
            page_path: "",
            title: "",
            format: "",
            commit_details: nil,
            content: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:page_path, 2, type: :bytes, json_name: "pagePath")
  field(:title, 3, type: :bytes)
  field(:format, 4, type: :string)
  field(:commit_details, 5, type: Gitaly.WikiCommitDetails, json_name: "commitDetails")
  field(:content, 6, type: :bytes)
end

defmodule Gitaly.WikiUpdatePageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          error: binary
        }

  defstruct error: ""

  field(:error, 1, type: :bytes)
end

defmodule Gitaly.WikiFindPageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          title: binary,
          revision: binary,
          directory: binary
        }

  defstruct repository: nil,
            title: "",
            revision: "",
            directory: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:title, 2, type: :bytes)
  field(:revision, 3, type: :bytes)
  field(:directory, 4, type: :bytes)
end

defmodule Gitaly.WikiFindPageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          page: Gitaly.WikiPage.t() | nil
        }

  defstruct page: nil

  field(:page, 1, type: Gitaly.WikiPage)
end

defmodule Gitaly.WikiGetAllPagesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          limit: non_neg_integer,
          direction_desc: boolean,
          sort: Gitaly.WikiGetAllPagesRequest.SortBy.t()
        }

  defstruct repository: nil,
            limit: 0,
            direction_desc: false,
            sort: :TITLE

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:limit, 2, type: :uint32)
  field(:direction_desc, 3, type: :bool, json_name: "directionDesc")
  field(:sort, 4, type: Gitaly.WikiGetAllPagesRequest.SortBy, enum: true)
end

defmodule Gitaly.WikiGetAllPagesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          page: Gitaly.WikiPage.t() | nil,
          end_of_page: boolean
        }

  defstruct page: nil,
            end_of_page: false

  field(:page, 1, type: Gitaly.WikiPage)
  field(:end_of_page, 2, type: :bool, json_name: "endOfPage")
end

defmodule Gitaly.WikiListPagesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          limit: non_neg_integer,
          direction_desc: boolean,
          sort: Gitaly.WikiListPagesRequest.SortBy.t(),
          offset: non_neg_integer
        }

  defstruct repository: nil,
            limit: 0,
            direction_desc: false,
            sort: :TITLE,
            offset: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:limit, 2, type: :uint32)
  field(:direction_desc, 3, type: :bool, json_name: "directionDesc")
  field(:sort, 4, type: Gitaly.WikiListPagesRequest.SortBy, enum: true)
  field(:offset, 5, type: :uint32)
end

defmodule Gitaly.WikiListPagesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          page: Gitaly.WikiPage.t() | nil
        }

  defstruct page: nil

  field(:page, 1, type: Gitaly.WikiPage)
end

defmodule Gitaly.WikiService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.WikiService"

  rpc(:WikiWritePage, stream(Gitaly.WikiWritePageRequest), Gitaly.WikiWritePageResponse)

  rpc(:WikiUpdatePage, stream(Gitaly.WikiUpdatePageRequest), Gitaly.WikiUpdatePageResponse)

  rpc(:WikiFindPage, Gitaly.WikiFindPageRequest, stream(Gitaly.WikiFindPageResponse))

  rpc(:WikiGetAllPages, Gitaly.WikiGetAllPagesRequest, stream(Gitaly.WikiGetAllPagesResponse))

  rpc(:WikiListPages, Gitaly.WikiListPagesRequest, stream(Gitaly.WikiListPagesResponse))
end

defmodule Gitaly.WikiService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.WikiService.Service
end
