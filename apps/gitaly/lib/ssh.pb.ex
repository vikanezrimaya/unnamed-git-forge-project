defmodule Gitaly.SSHUploadPackRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          stdin: binary,
          git_config_options: [String.t()],
          git_protocol: String.t()
        }

  defstruct repository: nil,
            stdin: "",
            git_config_options: [],
            git_protocol: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:stdin, 2, type: :bytes)
  field(:git_config_options, 4, repeated: true, type: :string, json_name: "gitConfigOptions")
  field(:git_protocol, 5, type: :string, json_name: "gitProtocol")
end

defmodule Gitaly.SSHUploadPackResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.SSHReceivePackRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          stdin: binary,
          gl_id: String.t(),
          gl_repository: String.t(),
          gl_username: String.t(),
          git_protocol: String.t(),
          git_config_options: [String.t()]
        }

  defstruct repository: nil,
            stdin: "",
            gl_id: "",
            gl_repository: "",
            gl_username: "",
            git_protocol: "",
            git_config_options: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:stdin, 2, type: :bytes)
  field(:gl_id, 3, type: :string, json_name: "glId")
  field(:gl_repository, 4, type: :string, json_name: "glRepository")
  field(:gl_username, 5, type: :string, json_name: "glUsername")
  field(:git_protocol, 6, type: :string, json_name: "gitProtocol")
  field(:git_config_options, 7, repeated: true, type: :string, json_name: "gitConfigOptions")
end

defmodule Gitaly.SSHReceivePackResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.SSHUploadArchiveRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          stdin: binary
        }

  defstruct repository: nil,
            stdin: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:stdin, 2, type: :bytes)
end

defmodule Gitaly.SSHUploadArchiveResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.SSHService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.SSHService"

  rpc(:SSHUploadPack, stream(Gitaly.SSHUploadPackRequest), stream(Gitaly.SSHUploadPackResponse))

  rpc(
    :SSHReceivePack,
    stream(Gitaly.SSHReceivePackRequest),
    stream(Gitaly.SSHReceivePackResponse)
  )

  rpc(
    :SSHUploadArchive,
    stream(Gitaly.SSHUploadArchiveRequest),
    stream(Gitaly.SSHUploadArchiveResponse)
  )
end

defmodule Gitaly.SSHService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.SSHService.Service
end
