defmodule Gitaly.CreateObjectPoolRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          object_pool: Gitaly.ObjectPool.t() | nil,
          origin: Gitaly.Repository.t() | nil
        }

  defstruct object_pool: nil,
            origin: nil

  field(:object_pool, 1, type: Gitaly.ObjectPool, json_name: "objectPool", deprecated: false)
  field(:origin, 2, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.CreateObjectPoolResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.DeleteObjectPoolRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          object_pool: Gitaly.ObjectPool.t() | nil
        }

  defstruct object_pool: nil

  field(:object_pool, 1, type: Gitaly.ObjectPool, json_name: "objectPool", deprecated: false)
end

defmodule Gitaly.DeleteObjectPoolResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.LinkRepositoryToObjectPoolRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          object_pool: Gitaly.ObjectPool.t() | nil,
          repository: Gitaly.Repository.t() | nil
        }

  defstruct object_pool: nil,
            repository: nil

  field(:object_pool, 1, type: Gitaly.ObjectPool, json_name: "objectPool", deprecated: false)
  field(:repository, 2, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.LinkRepositoryToObjectPoolResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.ReduplicateRepositoryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.ReduplicateRepositoryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.DisconnectGitAlternatesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.DisconnectGitAlternatesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FetchIntoObjectPoolRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          origin: Gitaly.Repository.t() | nil,
          object_pool: Gitaly.ObjectPool.t() | nil,
          repack: boolean
        }

  defstruct origin: nil,
            object_pool: nil,
            repack: false

  field(:origin, 1, type: Gitaly.Repository, deprecated: false)
  field(:object_pool, 2, type: Gitaly.ObjectPool, json_name: "objectPool", deprecated: false)
  field(:repack, 3, type: :bool)
end

defmodule Gitaly.FetchIntoObjectPoolResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.GetObjectPoolRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.GetObjectPoolResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          object_pool: Gitaly.ObjectPool.t() | nil
        }

  defstruct object_pool: nil

  field(:object_pool, 1, type: Gitaly.ObjectPool, json_name: "objectPool")
end

defmodule Gitaly.ObjectPoolService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.ObjectPoolService"

  rpc(:CreateObjectPool, Gitaly.CreateObjectPoolRequest, Gitaly.CreateObjectPoolResponse)

  rpc(:DeleteObjectPool, Gitaly.DeleteObjectPoolRequest, Gitaly.DeleteObjectPoolResponse)

  rpc(
    :LinkRepositoryToObjectPool,
    Gitaly.LinkRepositoryToObjectPoolRequest,
    Gitaly.LinkRepositoryToObjectPoolResponse
  )

  rpc(
    :ReduplicateRepository,
    Gitaly.ReduplicateRepositoryRequest,
    Gitaly.ReduplicateRepositoryResponse
  )

  rpc(
    :DisconnectGitAlternates,
    Gitaly.DisconnectGitAlternatesRequest,
    Gitaly.DisconnectGitAlternatesResponse
  )

  rpc(:FetchIntoObjectPool, Gitaly.FetchIntoObjectPoolRequest, Gitaly.FetchIntoObjectPoolResponse)

  rpc(:GetObjectPool, Gitaly.GetObjectPoolRequest, Gitaly.GetObjectPoolResponse)
end

defmodule Gitaly.ObjectPoolService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.ObjectPoolService.Service
end
