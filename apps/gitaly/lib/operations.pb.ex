defmodule Gitaly.UserCherryPickResponse.CreateTreeError do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :EMPTY | :CONFLICT

  field(:NONE, 0)
  field(:EMPTY, 1)
  field(:CONFLICT, 2)
end

defmodule Gitaly.UserRevertResponse.CreateTreeError do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :EMPTY | :CONFLICT

  field(:NONE, 0)
  field(:EMPTY, 1)
  field(:CONFLICT, 2)
end

defmodule Gitaly.UserCommitFilesActionHeader.ActionType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :CREATE | :CREATE_DIR | :UPDATE | :MOVE | :DELETE | :CHMOD

  field(:CREATE, 0)
  field(:CREATE_DIR, 1)
  field(:UPDATE, 2)
  field(:MOVE, 3)
  field(:DELETE, 4)
  field(:CHMOD, 5)
end

defmodule Gitaly.UserCreateBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          branch_name: binary,
          user: Gitaly.User.t() | nil,
          start_point: binary
        }

  defstruct repository: nil,
            branch_name: "",
            user: nil,
            start_point: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:branch_name, 2, type: :bytes, json_name: "branchName")
  field(:user, 3, type: Gitaly.User)
  field(:start_point, 4, type: :bytes, json_name: "startPoint")
end

defmodule Gitaly.UserCreateBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch: Gitaly.Branch.t() | nil,
          pre_receive_error: String.t()
        }

  defstruct branch: nil,
            pre_receive_error: ""

  field(:branch, 1, type: Gitaly.Branch)
  field(:pre_receive_error, 2, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserUpdateBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          branch_name: binary,
          user: Gitaly.User.t() | nil,
          newrev: binary,
          oldrev: binary
        }

  defstruct repository: nil,
            branch_name: "",
            user: nil,
            newrev: "",
            oldrev: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:branch_name, 2, type: :bytes, json_name: "branchName")
  field(:user, 3, type: Gitaly.User)
  field(:newrev, 4, type: :bytes)
  field(:oldrev, 5, type: :bytes)
end

defmodule Gitaly.UserUpdateBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pre_receive_error: String.t()
        }

  defstruct pre_receive_error: ""

  field(:pre_receive_error, 1, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserDeleteBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          branch_name: binary,
          user: Gitaly.User.t() | nil
        }

  defstruct repository: nil,
            branch_name: "",
            user: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:branch_name, 2, type: :bytes, json_name: "branchName")
  field(:user, 3, type: Gitaly.User)
end

defmodule Gitaly.UserDeleteBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pre_receive_error: String.t()
        }

  defstruct pre_receive_error: ""

  field(:pre_receive_error, 1, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserDeleteTagRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          tag_name: binary,
          user: Gitaly.User.t() | nil
        }

  defstruct repository: nil,
            tag_name: "",
            user: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:tag_name, 2, type: :bytes, json_name: "tagName")
  field(:user, 3, type: Gitaly.User)
end

defmodule Gitaly.UserDeleteTagResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pre_receive_error: String.t()
        }

  defstruct pre_receive_error: ""

  field(:pre_receive_error, 1, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserCreateTagRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          tag_name: binary,
          user: Gitaly.User.t() | nil,
          target_revision: binary,
          message: binary,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            tag_name: "",
            user: nil,
            target_revision: "",
            message: "",
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:tag_name, 2, type: :bytes, json_name: "tagName")
  field(:user, 3, type: Gitaly.User)
  field(:target_revision, 4, type: :bytes, json_name: "targetRevision")
  field(:message, 5, type: :bytes)
  field(:timestamp, 7, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserCreateTagResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tag: Gitaly.Tag.t() | nil,
          exists: boolean,
          pre_receive_error: String.t()
        }

  defstruct tag: nil,
            exists: false,
            pre_receive_error: ""

  field(:tag, 1, type: Gitaly.Tag)
  field(:exists, 2, type: :bool)
  field(:pre_receive_error, 3, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserMergeBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          commit_id: String.t(),
          branch: binary,
          message: binary,
          timestamp: Google.Protobuf.Timestamp.t() | nil,
          apply: boolean
        }

  defstruct repository: nil,
            user: nil,
            commit_id: "",
            branch: "",
            message: "",
            timestamp: nil,
            apply: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:commit_id, 3, type: :string, json_name: "commitId")
  field(:branch, 4, type: :bytes)
  field(:message, 5, type: :bytes)
  field(:timestamp, 7, type: Google.Protobuf.Timestamp)
  field(:apply, 6, type: :bool)
end

defmodule Gitaly.UserMergeBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_id: String.t(),
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          pre_receive_error: String.t()
        }

  defstruct commit_id: "",
            branch_update: nil,
            pre_receive_error: ""

  field(:commit_id, 1, type: :string, json_name: "commitId")
  field(:branch_update, 3, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:pre_receive_error, 4, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserMergeBranchError do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          error: {:access_check, Gitaly.AccessCheckError.t() | nil}
        }

  defstruct error: nil

  oneof(:error, 0)

  field(:access_check, 1, type: Gitaly.AccessCheckError, json_name: "accessCheck", oneof: 0)
end

defmodule Gitaly.UserMergeToRefRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          source_sha: String.t(),
          branch: binary,
          target_ref: binary,
          message: binary,
          first_parent_ref: binary,
          allow_conflicts: boolean,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            source_sha: "",
            branch: "",
            target_ref: "",
            message: "",
            first_parent_ref: "",
            allow_conflicts: false,
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:source_sha, 3, type: :string, json_name: "sourceSha")
  field(:branch, 4, type: :bytes)
  field(:target_ref, 5, type: :bytes, json_name: "targetRef")
  field(:message, 6, type: :bytes)
  field(:first_parent_ref, 7, type: :bytes, json_name: "firstParentRef")
  field(:allow_conflicts, 8, type: :bool, json_name: "allowConflicts")
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserMergeToRefResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_id: String.t(),
          pre_receive_error: String.t()
        }

  defstruct commit_id: "",
            pre_receive_error: ""

  field(:commit_id, 1, type: :string, json_name: "commitId")
  field(:pre_receive_error, 2, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.OperationBranchUpdate do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_id: String.t(),
          repo_created: boolean,
          branch_created: boolean
        }

  defstruct commit_id: "",
            repo_created: false,
            branch_created: false

  field(:commit_id, 1, type: :string, json_name: "commitId")
  field(:repo_created, 2, type: :bool, json_name: "repoCreated")
  field(:branch_created, 3, type: :bool, json_name: "branchCreated")
end

defmodule Gitaly.UserFFBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          commit_id: String.t(),
          branch: binary
        }

  defstruct repository: nil,
            user: nil,
            commit_id: "",
            branch: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:commit_id, 3, type: :string, json_name: "commitId")
  field(:branch, 4, type: :bytes)
end

defmodule Gitaly.UserFFBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          pre_receive_error: String.t()
        }

  defstruct branch_update: nil,
            pre_receive_error: ""

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:pre_receive_error, 2, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserCherryPickRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          commit: Gitaly.GitCommit.t() | nil,
          branch_name: binary,
          message: binary,
          start_branch_name: binary,
          start_repository: Gitaly.Repository.t() | nil,
          dry_run: boolean,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            commit: nil,
            branch_name: "",
            message: "",
            start_branch_name: "",
            start_repository: nil,
            dry_run: false,
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:commit, 3, type: Gitaly.GitCommit)
  field(:branch_name, 4, type: :bytes, json_name: "branchName")
  field(:message, 5, type: :bytes)
  field(:start_branch_name, 6, type: :bytes, json_name: "startBranchName")
  field(:start_repository, 7, type: Gitaly.Repository, json_name: "startRepository")
  field(:dry_run, 8, type: :bool, json_name: "dryRun")
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserCherryPickResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          create_tree_error: String.t(),
          commit_error: String.t(),
          pre_receive_error: String.t(),
          create_tree_error_code: Gitaly.UserCherryPickResponse.CreateTreeError.t()
        }

  defstruct branch_update: nil,
            create_tree_error: "",
            commit_error: "",
            pre_receive_error: "",
            create_tree_error_code: :NONE

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:create_tree_error, 2, type: :string, json_name: "createTreeError")
  field(:commit_error, 3, type: :string, json_name: "commitError")
  field(:pre_receive_error, 4, type: :string, json_name: "preReceiveError")

  field(:create_tree_error_code, 5,
    type: Gitaly.UserCherryPickResponse.CreateTreeError,
    json_name: "createTreeErrorCode",
    enum: true
  )
end

defmodule Gitaly.UserRevertRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          commit: Gitaly.GitCommit.t() | nil,
          branch_name: binary,
          message: binary,
          start_branch_name: binary,
          start_repository: Gitaly.Repository.t() | nil,
          dry_run: boolean,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            commit: nil,
            branch_name: "",
            message: "",
            start_branch_name: "",
            start_repository: nil,
            dry_run: false,
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:commit, 3, type: Gitaly.GitCommit)
  field(:branch_name, 4, type: :bytes, json_name: "branchName")
  field(:message, 5, type: :bytes)
  field(:start_branch_name, 6, type: :bytes, json_name: "startBranchName")
  field(:start_repository, 7, type: Gitaly.Repository, json_name: "startRepository")
  field(:dry_run, 8, type: :bool, json_name: "dryRun")
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserRevertResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          create_tree_error: String.t(),
          commit_error: String.t(),
          pre_receive_error: String.t(),
          create_tree_error_code: Gitaly.UserRevertResponse.CreateTreeError.t()
        }

  defstruct branch_update: nil,
            create_tree_error: "",
            commit_error: "",
            pre_receive_error: "",
            create_tree_error_code: :NONE

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:create_tree_error, 2, type: :string, json_name: "createTreeError")
  field(:commit_error, 3, type: :string, json_name: "commitError")
  field(:pre_receive_error, 4, type: :string, json_name: "preReceiveError")

  field(:create_tree_error_code, 5,
    type: Gitaly.UserRevertResponse.CreateTreeError,
    json_name: "createTreeErrorCode",
    enum: true
  )
end

defmodule Gitaly.UserCommitFilesActionHeader do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          action: Gitaly.UserCommitFilesActionHeader.ActionType.t(),
          file_path: binary,
          previous_path: binary,
          base64_content: boolean,
          execute_filemode: boolean,
          infer_content: boolean
        }

  defstruct action: :CREATE,
            file_path: "",
            previous_path: "",
            base64_content: false,
            execute_filemode: false,
            infer_content: false

  field(:action, 1, type: Gitaly.UserCommitFilesActionHeader.ActionType, enum: true)
  field(:file_path, 2, type: :bytes, json_name: "filePath")
  field(:previous_path, 3, type: :bytes, json_name: "previousPath")
  field(:base64_content, 4, type: :bool, json_name: "base64Content")
  field(:execute_filemode, 5, type: :bool, json_name: "executeFilemode")
  field(:infer_content, 6, type: :bool, json_name: "inferContent")
end

defmodule Gitaly.UserCommitFilesAction do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_commit_files_action_payload:
            {:header, Gitaly.UserCommitFilesActionHeader.t() | nil} | {:content, binary}
        }

  defstruct user_commit_files_action_payload: nil

  oneof(:user_commit_files_action_payload, 0)

  field(:header, 1, type: Gitaly.UserCommitFilesActionHeader, oneof: 0)
  field(:content, 2, type: :bytes, oneof: 0)
end

defmodule Gitaly.UserCommitFilesRequestHeader do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          branch_name: binary,
          commit_message: binary,
          commit_author_name: binary,
          commit_author_email: binary,
          start_branch_name: binary,
          start_repository: Gitaly.Repository.t() | nil,
          force: boolean,
          start_sha: String.t(),
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            branch_name: "",
            commit_message: "",
            commit_author_name: "",
            commit_author_email: "",
            start_branch_name: "",
            start_repository: nil,
            force: false,
            start_sha: "",
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:branch_name, 3, type: :bytes, json_name: "branchName")
  field(:commit_message, 4, type: :bytes, json_name: "commitMessage")
  field(:commit_author_name, 5, type: :bytes, json_name: "commitAuthorName")
  field(:commit_author_email, 6, type: :bytes, json_name: "commitAuthorEmail")
  field(:start_branch_name, 7, type: :bytes, json_name: "startBranchName")
  field(:start_repository, 8, type: Gitaly.Repository, json_name: "startRepository")
  field(:force, 9, type: :bool)
  field(:start_sha, 10, type: :string, json_name: "startSha")
  field(:timestamp, 11, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserCommitFilesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_commit_files_request_payload:
            {:header, Gitaly.UserCommitFilesRequestHeader.t() | nil}
            | {:action, Gitaly.UserCommitFilesAction.t() | nil}
        }

  defstruct user_commit_files_request_payload: nil

  oneof(:user_commit_files_request_payload, 0)

  field(:header, 1, type: Gitaly.UserCommitFilesRequestHeader, oneof: 0)
  field(:action, 2, type: Gitaly.UserCommitFilesAction, oneof: 0)
end

defmodule Gitaly.UserCommitFilesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          index_error: String.t(),
          pre_receive_error: String.t()
        }

  defstruct branch_update: nil,
            index_error: "",
            pre_receive_error: ""

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:index_error, 2, type: :string, json_name: "indexError")
  field(:pre_receive_error, 3, type: :string, json_name: "preReceiveError")
end

defmodule Gitaly.UserRebaseConfirmableRequest.Header do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          rebase_id: String.t(),
          branch: binary,
          branch_sha: String.t(),
          remote_repository: Gitaly.Repository.t() | nil,
          remote_branch: binary,
          git_push_options: [String.t()],
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            rebase_id: "",
            branch: "",
            branch_sha: "",
            remote_repository: nil,
            remote_branch: "",
            git_push_options: [],
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:rebase_id, 3, type: :string, json_name: "rebaseId")
  field(:branch, 4, type: :bytes)
  field(:branch_sha, 5, type: :string, json_name: "branchSha")
  field(:remote_repository, 6, type: Gitaly.Repository, json_name: "remoteRepository")
  field(:remote_branch, 7, type: :bytes, json_name: "remoteBranch")
  field(:git_push_options, 8, repeated: true, type: :string, json_name: "gitPushOptions")
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserRebaseConfirmableRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_rebase_confirmable_request_payload:
            {:header, Gitaly.UserRebaseConfirmableRequest.Header.t() | nil} | {:apply, boolean}
        }

  defstruct user_rebase_confirmable_request_payload: nil

  oneof(:user_rebase_confirmable_request_payload, 0)

  field(:header, 1, type: Gitaly.UserRebaseConfirmableRequest.Header, oneof: 0)
  field(:apply, 2, type: :bool, oneof: 0)
end

defmodule Gitaly.UserRebaseConfirmableResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_rebase_confirmable_response_payload:
            {:rebase_sha, String.t()} | {:rebase_applied, boolean},
          pre_receive_error: String.t(),
          git_error: String.t()
        }

  defstruct user_rebase_confirmable_response_payload: nil,
            pre_receive_error: "",
            git_error: ""

  oneof(:user_rebase_confirmable_response_payload, 0)

  field(:rebase_sha, 1, type: :string, json_name: "rebaseSha", oneof: 0)
  field(:rebase_applied, 2, type: :bool, json_name: "rebaseApplied", oneof: 0)
  field(:pre_receive_error, 3, type: :string, json_name: "preReceiveError")
  field(:git_error, 4, type: :string, json_name: "gitError")
end

defmodule Gitaly.UserSquashRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          squash_id: String.t(),
          start_sha: String.t(),
          end_sha: String.t(),
          author: Gitaly.User.t() | nil,
          commit_message: binary,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            squash_id: "",
            start_sha: "",
            end_sha: "",
            author: nil,
            commit_message: "",
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:squash_id, 3, type: :string, json_name: "squashId", deprecated: true)
  field(:start_sha, 5, type: :string, json_name: "startSha")
  field(:end_sha, 6, type: :string, json_name: "endSha")
  field(:author, 7, type: Gitaly.User)
  field(:commit_message, 8, type: :bytes, json_name: "commitMessage")
  field(:timestamp, 9, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserSquashResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          squash_sha: String.t(),
          git_error: String.t()
        }

  defstruct squash_sha: "",
            git_error: ""

  field(:squash_sha, 1, type: :string, json_name: "squashSha")
  field(:git_error, 3, type: :string, json_name: "gitError")
end

defmodule Gitaly.UserApplyPatchRequest.Header do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          target_branch: binary,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            target_branch: "",
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:target_branch, 3, type: :bytes, json_name: "targetBranch")
  field(:timestamp, 4, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserApplyPatchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_apply_patch_request_payload:
            {:header, Gitaly.UserApplyPatchRequest.Header.t() | nil} | {:patches, binary}
        }

  defstruct user_apply_patch_request_payload: nil

  oneof(:user_apply_patch_request_payload, 0)

  field(:header, 1, type: Gitaly.UserApplyPatchRequest.Header, oneof: 0)
  field(:patches, 2, type: :bytes, oneof: 0)
end

defmodule Gitaly.UserApplyPatchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil
        }

  defstruct branch_update: nil

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
end

defmodule Gitaly.UserUpdateSubmoduleRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          user: Gitaly.User.t() | nil,
          commit_sha: String.t(),
          branch: binary,
          submodule: binary,
          commit_message: binary,
          timestamp: Google.Protobuf.Timestamp.t() | nil
        }

  defstruct repository: nil,
            user: nil,
            commit_sha: "",
            branch: "",
            submodule: "",
            commit_message: "",
            timestamp: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:user, 2, type: Gitaly.User)
  field(:commit_sha, 3, type: :string, json_name: "commitSha")
  field(:branch, 4, type: :bytes)
  field(:submodule, 5, type: :bytes)
  field(:commit_message, 6, type: :bytes, json_name: "commitMessage")
  field(:timestamp, 7, type: Google.Protobuf.Timestamp)
end

defmodule Gitaly.UserUpdateSubmoduleResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_update: Gitaly.OperationBranchUpdate.t() | nil,
          pre_receive_error: String.t(),
          commit_error: String.t()
        }

  defstruct branch_update: nil,
            pre_receive_error: "",
            commit_error: ""

  field(:branch_update, 1, type: Gitaly.OperationBranchUpdate, json_name: "branchUpdate")
  field(:pre_receive_error, 2, type: :string, json_name: "preReceiveError")
  field(:commit_error, 4, type: :string, json_name: "commitError")
end

defmodule Gitaly.OperationService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.OperationService"

  rpc(:UserCreateBranch, Gitaly.UserCreateBranchRequest, Gitaly.UserCreateBranchResponse)

  rpc(:UserUpdateBranch, Gitaly.UserUpdateBranchRequest, Gitaly.UserUpdateBranchResponse)

  rpc(:UserDeleteBranch, Gitaly.UserDeleteBranchRequest, Gitaly.UserDeleteBranchResponse)

  rpc(:UserCreateTag, Gitaly.UserCreateTagRequest, Gitaly.UserCreateTagResponse)

  rpc(:UserDeleteTag, Gitaly.UserDeleteTagRequest, Gitaly.UserDeleteTagResponse)

  rpc(:UserMergeToRef, Gitaly.UserMergeToRefRequest, Gitaly.UserMergeToRefResponse)

  rpc(
    :UserMergeBranch,
    stream(Gitaly.UserMergeBranchRequest),
    stream(Gitaly.UserMergeBranchResponse)
  )

  rpc(:UserFFBranch, Gitaly.UserFFBranchRequest, Gitaly.UserFFBranchResponse)

  rpc(:UserCherryPick, Gitaly.UserCherryPickRequest, Gitaly.UserCherryPickResponse)

  rpc(:UserCommitFiles, stream(Gitaly.UserCommitFilesRequest), Gitaly.UserCommitFilesResponse)

  rpc(
    :UserRebaseConfirmable,
    stream(Gitaly.UserRebaseConfirmableRequest),
    stream(Gitaly.UserRebaseConfirmableResponse)
  )

  rpc(:UserRevert, Gitaly.UserRevertRequest, Gitaly.UserRevertResponse)

  rpc(:UserSquash, Gitaly.UserSquashRequest, Gitaly.UserSquashResponse)

  rpc(:UserApplyPatch, stream(Gitaly.UserApplyPatchRequest), Gitaly.UserApplyPatchResponse)

  rpc(:UserUpdateSubmodule, Gitaly.UserUpdateSubmoduleRequest, Gitaly.UserUpdateSubmoduleResponse)
end

defmodule Gitaly.OperationService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.OperationService.Service
end
