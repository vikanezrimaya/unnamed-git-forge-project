defmodule Gitaly.ObjectType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :UNKNOWN | :COMMIT | :BLOB | :TREE | :TAG

  field(:UNKNOWN, 0)
  field(:COMMIT, 1)
  field(:BLOB, 2)
  field(:TREE, 3)
  field(:TAG, 4)
end

defmodule Gitaly.SignatureType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :PGP | :X509

  field(:NONE, 0)
  field(:PGP, 1)
  field(:X509, 2)
end

defmodule Gitaly.SortDirection do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :ASCENDING | :DESCENDING

  field(:ASCENDING, 0)
  field(:DESCENDING, 1)
end

defmodule Gitaly.Repository do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t(),
          relative_path: String.t(),
          git_object_directory: String.t(),
          git_alternate_object_directories: [String.t()],
          gl_repository: String.t(),
          gl_project_path: String.t()
        }

  defstruct storage_name: "",
            relative_path: "",
            git_object_directory: "",
            git_alternate_object_directories: [],
            gl_repository: "",
            gl_project_path: ""

  field(:storage_name, 2, type: :string, json_name: "storageName")
  field(:relative_path, 3, type: :string, json_name: "relativePath")
  field(:git_object_directory, 4, type: :string, json_name: "gitObjectDirectory")

  field(:git_alternate_object_directories, 5,
    repeated: true,
    type: :string,
    json_name: "gitAlternateObjectDirectories"
  )

  field(:gl_repository, 6, type: :string, json_name: "glRepository")
  field(:gl_project_path, 8, type: :string, json_name: "glProjectPath")
end

defmodule Gitaly.CommitTrailer do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          key: binary,
          value: binary
        }

  defstruct key: "",
            value: ""

  field(:key, 1, type: :bytes)
  field(:value, 2, type: :bytes)
end

defmodule Gitaly.GitCommit do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: String.t(),
          subject: binary,
          body: binary,
          author: Gitaly.CommitAuthor.t() | nil,
          committer: Gitaly.CommitAuthor.t() | nil,
          parent_ids: [String.t()],
          body_size: integer,
          signature_type: Gitaly.SignatureType.t(),
          tree_id: String.t(),
          trailers: [Gitaly.CommitTrailer.t()]
        }

  defstruct id: "",
            subject: "",
            body: "",
            author: nil,
            committer: nil,
            parent_ids: [],
            body_size: 0,
            signature_type: :NONE,
            tree_id: "",
            trailers: []

  field(:id, 1, type: :string)
  field(:subject, 2, type: :bytes)
  field(:body, 3, type: :bytes)
  field(:author, 4, type: Gitaly.CommitAuthor)
  field(:committer, 5, type: Gitaly.CommitAuthor)
  field(:parent_ids, 6, repeated: true, type: :string, json_name: "parentIds")
  field(:body_size, 7, type: :int64, json_name: "bodySize")
  field(:signature_type, 8, type: Gitaly.SignatureType, json_name: "signatureType", enum: true)
  field(:tree_id, 9, type: :string, json_name: "treeId")
  field(:trailers, 10, repeated: true, type: Gitaly.CommitTrailer)
end

defmodule Gitaly.CommitAuthor do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          email: binary,
          date: Google.Protobuf.Timestamp.t() | nil,
          timezone: binary
        }

  defstruct name: "",
            email: "",
            date: nil,
            timezone: ""

  field(:name, 1, type: :bytes)
  field(:email, 2, type: :bytes)
  field(:date, 3, type: Google.Protobuf.Timestamp)
  field(:timezone, 4, type: :bytes)
end

defmodule Gitaly.ExitStatus do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          value: integer
        }

  defstruct value: 0

  field(:value, 1, type: :int32)
end

defmodule Gitaly.Branch do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          target_commit: Gitaly.GitCommit.t() | nil
        }

  defstruct name: "",
            target_commit: nil

  field(:name, 1, type: :bytes)
  field(:target_commit, 2, type: Gitaly.GitCommit, json_name: "targetCommit")
end

defmodule Gitaly.Tag do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          id: String.t(),
          target_commit: Gitaly.GitCommit.t() | nil,
          message: binary,
          message_size: integer,
          tagger: Gitaly.CommitAuthor.t() | nil,
          signature_type: Gitaly.SignatureType.t()
        }

  defstruct name: "",
            id: "",
            target_commit: nil,
            message: "",
            message_size: 0,
            tagger: nil,
            signature_type: :NONE

  field(:name, 1, type: :bytes)
  field(:id, 2, type: :string)
  field(:target_commit, 3, type: Gitaly.GitCommit, json_name: "targetCommit")
  field(:message, 4, type: :bytes)
  field(:message_size, 5, type: :int64, json_name: "messageSize")
  field(:tagger, 6, type: Gitaly.CommitAuthor)
  field(:signature_type, 7, type: Gitaly.SignatureType, json_name: "signatureType", enum: true)
end

defmodule Gitaly.User do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          gl_id: String.t(),
          name: binary,
          email: binary,
          gl_username: String.t(),
          timezone: String.t()
        }

  defstruct gl_id: "",
            name: "",
            email: "",
            gl_username: "",
            timezone: ""

  field(:gl_id, 1, type: :string, json_name: "glId")
  field(:name, 2, type: :bytes)
  field(:email, 3, type: :bytes)
  field(:gl_username, 4, type: :string, json_name: "glUsername")
  field(:timezone, 5, type: :string)
end

defmodule Gitaly.ObjectPool do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.PaginationParameter do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          page_token: String.t(),
          limit: integer
        }

  defstruct page_token: "",
            limit: 0

  field(:page_token, 1, type: :string, json_name: "pageToken")
  field(:limit, 2, type: :int32)
end

defmodule Gitaly.PaginationCursor do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          next_cursor: String.t()
        }

  defstruct next_cursor: ""

  field(:next_cursor, 1, type: :string, json_name: "nextCursor")
end

defmodule Gitaly.GlobalOptions do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          literal_pathspecs: boolean
        }

  defstruct literal_pathspecs: false

  field(:literal_pathspecs, 1, type: :bool, json_name: "literalPathspecs")
end
