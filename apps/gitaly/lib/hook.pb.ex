defmodule Gitaly.ReferenceTransactionHookRequest.State do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :PREPARED | :COMMITTED | :ABORTED

  field(:PREPARED, 0)
  field(:COMMITTED, 1)
  field(:ABORTED, 2)
end

defmodule Gitaly.PreReceiveHookRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          environment_variables: [String.t()],
          stdin: binary,
          git_push_options: [String.t()]
        }

  defstruct repository: nil,
            environment_variables: [],
            stdin: "",
            git_push_options: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:environment_variables, 2,
    repeated: true,
    type: :string,
    json_name: "environmentVariables"
  )

  field(:stdin, 4, type: :bytes)
  field(:git_push_options, 5, repeated: true, type: :string, json_name: "gitPushOptions")
end

defmodule Gitaly.PreReceiveHookResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.PostReceiveHookRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          environment_variables: [String.t()],
          stdin: binary,
          git_push_options: [String.t()]
        }

  defstruct repository: nil,
            environment_variables: [],
            stdin: "",
            git_push_options: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:environment_variables, 2,
    repeated: true,
    type: :string,
    json_name: "environmentVariables"
  )

  field(:stdin, 3, type: :bytes)
  field(:git_push_options, 4, repeated: true, type: :string, json_name: "gitPushOptions")
end

defmodule Gitaly.PostReceiveHookResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.UpdateHookRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          environment_variables: [String.t()],
          ref: binary,
          old_value: String.t(),
          new_value: String.t()
        }

  defstruct repository: nil,
            environment_variables: [],
            ref: "",
            old_value: "",
            new_value: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:environment_variables, 2,
    repeated: true,
    type: :string,
    json_name: "environmentVariables"
  )

  field(:ref, 3, type: :bytes)
  field(:old_value, 4, type: :string, json_name: "oldValue")
  field(:new_value, 5, type: :string, json_name: "newValue")
end

defmodule Gitaly.UpdateHookResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.ReferenceTransactionHookRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          environment_variables: [String.t()],
          stdin: binary,
          state: Gitaly.ReferenceTransactionHookRequest.State.t()
        }

  defstruct repository: nil,
            environment_variables: [],
            stdin: "",
            state: :PREPARED

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:environment_variables, 2,
    repeated: true,
    type: :string,
    json_name: "environmentVariables"
  )

  field(:stdin, 3, type: :bytes)
  field(:state, 4, type: Gitaly.ReferenceTransactionHookRequest.State, enum: true)
end

defmodule Gitaly.ReferenceTransactionHookResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary,
          exit_status: Gitaly.ExitStatus.t() | nil
        }

  defstruct stdout: "",
            stderr: "",
            exit_status: nil

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
  field(:exit_status, 3, type: Gitaly.ExitStatus, json_name: "exitStatus")
end

defmodule Gitaly.PackObjectsHookRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          args: [String.t()],
          stdin: binary
        }

  defstruct repository: nil,
            args: [],
            stdin: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:args, 2, repeated: true, type: :string)
  field(:stdin, 3, type: :bytes)
end

defmodule Gitaly.PackObjectsHookResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          stdout: binary,
          stderr: binary
        }

  defstruct stdout: "",
            stderr: ""

  field(:stdout, 1, type: :bytes)
  field(:stderr, 2, type: :bytes)
end

defmodule Gitaly.PackObjectsHookWithSidechannelRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          args: [String.t()]
        }

  defstruct repository: nil,
            args: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:args, 2, repeated: true, type: :string)
end

defmodule Gitaly.PackObjectsHookWithSidechannelResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.HookService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.HookService"

  rpc(
    :PreReceiveHook,
    stream(Gitaly.PreReceiveHookRequest),
    stream(Gitaly.PreReceiveHookResponse)
  )

  rpc(
    :PostReceiveHook,
    stream(Gitaly.PostReceiveHookRequest),
    stream(Gitaly.PostReceiveHookResponse)
  )

  rpc(:UpdateHook, Gitaly.UpdateHookRequest, stream(Gitaly.UpdateHookResponse))

  rpc(
    :ReferenceTransactionHook,
    stream(Gitaly.ReferenceTransactionHookRequest),
    stream(Gitaly.ReferenceTransactionHookResponse)
  )

  rpc(
    :PackObjectsHook,
    stream(Gitaly.PackObjectsHookRequest),
    stream(Gitaly.PackObjectsHookResponse)
  )

  rpc(
    :PackObjectsHookWithSidechannel,
    Gitaly.PackObjectsHookWithSidechannelRequest,
    Gitaly.PackObjectsHookWithSidechannelResponse
  )
end

defmodule Gitaly.HookService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.HookService.Service
end
