defmodule Gitaly.GetBlobRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          oid: String.t(),
          limit: integer
        }

  defstruct repository: nil,
            oid: "",
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:oid, 2, type: :string)
  field(:limit, 3, type: :int64)
end

defmodule Gitaly.GetBlobResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer,
          data: binary,
          oid: String.t()
        }

  defstruct size: 0,
            data: "",
            oid: ""

  field(:size, 1, type: :int64)
  field(:data, 2, type: :bytes)
  field(:oid, 3, type: :string)
end

defmodule Gitaly.GetBlobsRequest.RevisionPath do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          revision: String.t(),
          path: binary
        }

  defstruct revision: "",
            path: ""

  field(:revision, 1, type: :string)
  field(:path, 2, type: :bytes)
end

defmodule Gitaly.GetBlobsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision_paths: [Gitaly.GetBlobsRequest.RevisionPath.t()],
          limit: integer
        }

  defstruct repository: nil,
            revision_paths: [],
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:revision_paths, 2,
    repeated: true,
    type: Gitaly.GetBlobsRequest.RevisionPath,
    json_name: "revisionPaths"
  )

  field(:limit, 3, type: :int64)
end

defmodule Gitaly.GetBlobsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer,
          data: binary,
          oid: String.t(),
          is_submodule: boolean,
          mode: integer,
          revision: String.t(),
          path: binary,
          type: Gitaly.ObjectType.t()
        }

  defstruct size: 0,
            data: "",
            oid: "",
            is_submodule: false,
            mode: 0,
            revision: "",
            path: "",
            type: :UNKNOWN

  field(:size, 1, type: :int64)
  field(:data, 2, type: :bytes)
  field(:oid, 3, type: :string)
  field(:is_submodule, 4, type: :bool, json_name: "isSubmodule")
  field(:mode, 5, type: :int32)
  field(:revision, 6, type: :string)
  field(:path, 7, type: :bytes)
  field(:type, 8, type: Gitaly.ObjectType, enum: true)
end

defmodule Gitaly.ListBlobsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revisions: [String.t()],
          limit: non_neg_integer,
          bytes_limit: integer,
          with_paths: boolean
        }

  defstruct repository: nil,
            revisions: [],
            limit: 0,
            bytes_limit: 0,
            with_paths: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revisions, 2, repeated: true, type: :string)
  field(:limit, 3, type: :uint32)
  field(:bytes_limit, 4, type: :int64, json_name: "bytesLimit")
  field(:with_paths, 5, type: :bool, json_name: "withPaths")
end

defmodule Gitaly.ListBlobsResponse.Blob do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          oid: String.t(),
          size: integer,
          data: binary,
          path: binary
        }

  defstruct oid: "",
            size: 0,
            data: "",
            path: ""

  field(:oid, 1, type: :string)
  field(:size, 2, type: :int64)
  field(:data, 3, type: :bytes)
  field(:path, 4, type: :bytes)
end

defmodule Gitaly.ListBlobsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          blobs: [Gitaly.ListBlobsResponse.Blob.t()]
        }

  defstruct blobs: []

  field(:blobs, 1, repeated: true, type: Gitaly.ListBlobsResponse.Blob)
end

defmodule Gitaly.ListAllBlobsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          limit: non_neg_integer,
          bytes_limit: integer
        }

  defstruct repository: nil,
            limit: 0,
            bytes_limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:limit, 2, type: :uint32)
  field(:bytes_limit, 3, type: :int64, json_name: "bytesLimit")
end

defmodule Gitaly.ListAllBlobsResponse.Blob do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          oid: String.t(),
          size: integer,
          data: binary
        }

  defstruct oid: "",
            size: 0,
            data: ""

  field(:oid, 1, type: :string)
  field(:size, 2, type: :int64)
  field(:data, 3, type: :bytes)
end

defmodule Gitaly.ListAllBlobsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          blobs: [Gitaly.ListAllBlobsResponse.Blob.t()]
        }

  defstruct blobs: []

  field(:blobs, 1, repeated: true, type: Gitaly.ListAllBlobsResponse.Blob)
end

defmodule Gitaly.LFSPointer do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer,
          data: binary,
          oid: String.t()
        }

  defstruct size: 0,
            data: "",
            oid: ""

  field(:size, 1, type: :int64)
  field(:data, 2, type: :bytes)
  field(:oid, 3, type: :string)
end

defmodule Gitaly.NewBlobObject do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          size: integer,
          oid: String.t(),
          path: binary
        }

  defstruct size: 0,
            oid: "",
            path: ""

  field(:size, 1, type: :int64)
  field(:oid, 2, type: :string)
  field(:path, 3, type: :bytes)
end

defmodule Gitaly.GetLFSPointersRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          blob_ids: [String.t()]
        }

  defstruct repository: nil,
            blob_ids: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:blob_ids, 2, repeated: true, type: :string, json_name: "blobIds")
end

defmodule Gitaly.GetLFSPointersResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          lfs_pointers: [Gitaly.LFSPointer.t()]
        }

  defstruct lfs_pointers: []

  field(:lfs_pointers, 1, repeated: true, type: Gitaly.LFSPointer, json_name: "lfsPointers")
end

defmodule Gitaly.ListLFSPointersRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revisions: [String.t()],
          limit: integer
        }

  defstruct repository: nil,
            revisions: [],
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revisions, 2, repeated: true, type: :string)
  field(:limit, 3, type: :int32)
end

defmodule Gitaly.ListLFSPointersResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          lfs_pointers: [Gitaly.LFSPointer.t()]
        }

  defstruct lfs_pointers: []

  field(:lfs_pointers, 1, repeated: true, type: Gitaly.LFSPointer, json_name: "lfsPointers")
end

defmodule Gitaly.ListAllLFSPointersRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          limit: integer
        }

  defstruct repository: nil,
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:limit, 3, type: :int32)
end

defmodule Gitaly.ListAllLFSPointersResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          lfs_pointers: [Gitaly.LFSPointer.t()]
        }

  defstruct lfs_pointers: []

  field(:lfs_pointers, 1, repeated: true, type: Gitaly.LFSPointer, json_name: "lfsPointers")
end

defmodule Gitaly.BlobService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.BlobService"

  rpc(:GetBlob, Gitaly.GetBlobRequest, stream(Gitaly.GetBlobResponse))

  rpc(:GetBlobs, Gitaly.GetBlobsRequest, stream(Gitaly.GetBlobsResponse))

  rpc(:ListBlobs, Gitaly.ListBlobsRequest, stream(Gitaly.ListBlobsResponse))

  rpc(:ListAllBlobs, Gitaly.ListAllBlobsRequest, stream(Gitaly.ListAllBlobsResponse))

  rpc(:GetLFSPointers, Gitaly.GetLFSPointersRequest, stream(Gitaly.GetLFSPointersResponse))

  rpc(:ListLFSPointers, Gitaly.ListLFSPointersRequest, stream(Gitaly.ListLFSPointersResponse))

  rpc(
    :ListAllLFSPointers,
    Gitaly.ListAllLFSPointersRequest,
    stream(Gitaly.ListAllLFSPointersResponse)
  )
end

defmodule Gitaly.BlobService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.BlobService.Service
end
