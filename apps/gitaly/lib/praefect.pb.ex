defmodule Gitaly.GetRepositoryMetadataRequest.Path do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          virtual_storage: String.t(),
          relative_path: String.t()
        }

  defstruct virtual_storage: "",
            relative_path: ""

  field(:virtual_storage, 1, type: :string, json_name: "virtualStorage")
  field(:relative_path, 2, type: :string, json_name: "relativePath")
end

defmodule Gitaly.GetRepositoryMetadataRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          query:
            {:repository_id, integer}
            | {:path, Gitaly.GetRepositoryMetadataRequest.Path.t() | nil}
        }

  defstruct query: nil

  oneof(:query, 0)

  field(:repository_id, 1, type: :int64, json_name: "repositoryId", oneof: 0)
  field(:path, 2, type: Gitaly.GetRepositoryMetadataRequest.Path, oneof: 0)
end

defmodule Gitaly.GetRepositoryMetadataResponse.Replica do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage: String.t(),
          assigned: boolean,
          generation: integer,
          healthy: boolean,
          valid_primary: boolean
        }

  defstruct storage: "",
            assigned: false,
            generation: 0,
            healthy: false,
            valid_primary: false

  field(:storage, 1, type: :string)
  field(:assigned, 2, type: :bool)
  field(:generation, 4, type: :int64)
  field(:healthy, 5, type: :bool)
  field(:valid_primary, 6, type: :bool, json_name: "validPrimary")
end

defmodule Gitaly.GetRepositoryMetadataResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository_id: integer,
          virtual_storage: String.t(),
          relative_path: String.t(),
          replica_path: String.t(),
          primary: String.t(),
          generation: integer,
          replicas: [Gitaly.GetRepositoryMetadataResponse.Replica.t()]
        }

  defstruct repository_id: 0,
            virtual_storage: "",
            relative_path: "",
            replica_path: "",
            primary: "",
            generation: 0,
            replicas: []

  field(:repository_id, 1, type: :int64, json_name: "repositoryId")
  field(:virtual_storage, 2, type: :string, json_name: "virtualStorage")
  field(:relative_path, 3, type: :string, json_name: "relativePath")
  field(:replica_path, 4, type: :string, json_name: "replicaPath")
  field(:primary, 5, type: :string)
  field(:generation, 6, type: :int64)
  field(:replicas, 7, repeated: true, type: Gitaly.GetRepositoryMetadataResponse.Replica)
end

defmodule Gitaly.SetReplicationFactorRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          virtual_storage: String.t(),
          relative_path: String.t(),
          replication_factor: integer
        }

  defstruct virtual_storage: "",
            relative_path: "",
            replication_factor: 0

  field(:virtual_storage, 1, type: :string, json_name: "virtualStorage")
  field(:relative_path, 2, type: :string, json_name: "relativePath")
  field(:replication_factor, 3, type: :int32, json_name: "replicationFactor")
end

defmodule Gitaly.SetReplicationFactorResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storages: [String.t()]
        }

  defstruct storages: []

  field(:storages, 1, repeated: true, type: :string)
end

defmodule Gitaly.SetAuthoritativeStorageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          virtual_storage: String.t(),
          relative_path: String.t(),
          authoritative_storage: String.t()
        }

  defstruct virtual_storage: "",
            relative_path: "",
            authoritative_storage: ""

  field(:virtual_storage, 1, type: :string, json_name: "virtualStorage")
  field(:relative_path, 2, type: :string, json_name: "relativePath")
  field(:authoritative_storage, 3, type: :string, json_name: "authoritativeStorage")
end

defmodule Gitaly.SetAuthoritativeStorageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.DatalossCheckRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          virtual_storage: String.t(),
          include_partially_replicated: boolean
        }

  defstruct virtual_storage: "",
            include_partially_replicated: false

  field(:virtual_storage, 1, type: :string, json_name: "virtualStorage")
  field(:include_partially_replicated, 2, type: :bool, json_name: "includePartiallyReplicated")
end

defmodule Gitaly.DatalossCheckResponse.Repository.Storage do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: String.t(),
          behind_by: integer,
          assigned: boolean,
          healthy: boolean,
          valid_primary: boolean
        }

  defstruct name: "",
            behind_by: 0,
            assigned: false,
            healthy: false,
            valid_primary: false

  field(:name, 1, type: :string)
  field(:behind_by, 2, type: :int64, json_name: "behindBy")
  field(:assigned, 3, type: :bool)
  field(:healthy, 4, type: :bool)
  field(:valid_primary, 5, type: :bool, json_name: "validPrimary")
end

defmodule Gitaly.DatalossCheckResponse.Repository do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          relative_path: String.t(),
          storages: [Gitaly.DatalossCheckResponse.Repository.Storage.t()],
          unavailable: boolean,
          primary: String.t()
        }

  defstruct relative_path: "",
            storages: [],
            unavailable: false,
            primary: ""

  field(:relative_path, 1, type: :string, json_name: "relativePath")
  field(:storages, 2, repeated: true, type: Gitaly.DatalossCheckResponse.Repository.Storage)
  field(:unavailable, 3, type: :bool)
  field(:primary, 4, type: :string)
end

defmodule Gitaly.DatalossCheckResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repositories: [Gitaly.DatalossCheckResponse.Repository.t()]
        }

  defstruct repositories: []

  field(:repositories, 2, repeated: true, type: Gitaly.DatalossCheckResponse.Repository)
end

defmodule Gitaly.RepositoryReplicasRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository)
end

defmodule Gitaly.RepositoryReplicasResponse.RepositoryDetails do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          checksum: String.t()
        }

  defstruct repository: nil,
            checksum: ""

  field(:repository, 1, type: Gitaly.Repository)
  field(:checksum, 2, type: :string)
end

defmodule Gitaly.RepositoryReplicasResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          primary: Gitaly.RepositoryReplicasResponse.RepositoryDetails.t() | nil,
          replicas: [Gitaly.RepositoryReplicasResponse.RepositoryDetails.t()]
        }

  defstruct primary: nil,
            replicas: []

  field(:primary, 1, type: Gitaly.RepositoryReplicasResponse.RepositoryDetails)
  field(:replicas, 2, repeated: true, type: Gitaly.RepositoryReplicasResponse.RepositoryDetails)
end

defmodule Gitaly.PraefectInfoService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.PraefectInfoService"

  rpc(:RepositoryReplicas, Gitaly.RepositoryReplicasRequest, Gitaly.RepositoryReplicasResponse)

  rpc(:DatalossCheck, Gitaly.DatalossCheckRequest, Gitaly.DatalossCheckResponse)

  rpc(
    :SetAuthoritativeStorage,
    Gitaly.SetAuthoritativeStorageRequest,
    Gitaly.SetAuthoritativeStorageResponse
  )

  rpc(
    :SetReplicationFactor,
    Gitaly.SetReplicationFactorRequest,
    Gitaly.SetReplicationFactorResponse
  )

  rpc(
    :GetRepositoryMetadata,
    Gitaly.GetRepositoryMetadataRequest,
    Gitaly.GetRepositoryMetadataResponse
  )
end

defmodule Gitaly.PraefectInfoService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.PraefectInfoService.Service
end
