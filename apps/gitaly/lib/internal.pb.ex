defmodule Gitaly.WalkReposRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          storage_name: String.t()
        }

  defstruct storage_name: ""

  field(:storage_name, 1, type: :string, json_name: "storageName", deprecated: false)
end

defmodule Gitaly.WalkReposResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          relative_path: String.t()
        }

  defstruct relative_path: ""

  field(:relative_path, 1, type: :string, json_name: "relativePath")
end

defmodule Gitaly.InternalGitaly.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.InternalGitaly"

  rpc(:WalkRepos, Gitaly.WalkReposRequest, stream(Gitaly.WalkReposResponse))
end

defmodule Gitaly.InternalGitaly.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.InternalGitaly.Service
end
