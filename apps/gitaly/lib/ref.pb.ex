defmodule Gitaly.FindLocalBranchesRequest.SortBy do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NAME | :UPDATED_ASC | :UPDATED_DESC

  field(:NAME, 0)
  field(:UPDATED_ASC, 1)
  field(:UPDATED_DESC, 2)
end

defmodule Gitaly.FindAllTagsRequest.SortBy.Key do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :REFNAME | :CREATORDATE

  field(:REFNAME, 0)
  field(:CREATORDATE, 1)
end

defmodule Gitaly.CreateBranchResponse.Status do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :OK | :ERR_EXISTS | :ERR_INVALID | :ERR_INVALID_START_POINT

  field(:OK, 0)
  field(:ERR_EXISTS, 1)
  field(:ERR_INVALID, 2)
  field(:ERR_INVALID_START_POINT, 3)
end

defmodule Gitaly.FindDefaultBranchNameRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.FindDefaultBranchNameResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary
        }

  defstruct name: ""

  field(:name, 1, type: :bytes)
end

defmodule Gitaly.FindAllBranchNamesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.FindAllBranchNamesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          names: [binary]
        }

  defstruct names: []

  field(:names, 1, repeated: true, type: :bytes)
end

defmodule Gitaly.FindAllTagNamesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil
        }

  defstruct repository: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
end

defmodule Gitaly.FindAllTagNamesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          names: [binary]
        }

  defstruct names: []

  field(:names, 1, repeated: true, type: :bytes)
end

defmodule Gitaly.FindLocalBranchesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          sort_by: Gitaly.FindLocalBranchesRequest.SortBy.t(),
          pagination_params: Gitaly.PaginationParameter.t() | nil
        }

  defstruct repository: nil,
            sort_by: :NAME,
            pagination_params: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)

  field(:sort_by, 2, type: Gitaly.FindLocalBranchesRequest.SortBy, json_name: "sortBy", enum: true)

  field(:pagination_params, 3, type: Gitaly.PaginationParameter, json_name: "paginationParams")
end

defmodule Gitaly.FindLocalBranchesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branches: [Gitaly.FindLocalBranchResponse.t()]
        }

  defstruct branches: []

  field(:branches, 1, repeated: true, type: Gitaly.FindLocalBranchResponse)
end

defmodule Gitaly.FindLocalBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          commit_id: String.t(),
          commit_subject: binary,
          commit_author: Gitaly.FindLocalBranchCommitAuthor.t() | nil,
          commit_committer: Gitaly.FindLocalBranchCommitAuthor.t() | nil,
          commit: Gitaly.GitCommit.t() | nil
        }

  defstruct name: "",
            commit_id: "",
            commit_subject: "",
            commit_author: nil,
            commit_committer: nil,
            commit: nil

  field(:name, 1, type: :bytes)
  field(:commit_id, 2, type: :string, json_name: "commitId")
  field(:commit_subject, 3, type: :bytes, json_name: "commitSubject")
  field(:commit_author, 4, type: Gitaly.FindLocalBranchCommitAuthor, json_name: "commitAuthor")

  field(:commit_committer, 5,
    type: Gitaly.FindLocalBranchCommitAuthor,
    json_name: "commitCommitter"
  )

  field(:commit, 6, type: Gitaly.GitCommit)
end

defmodule Gitaly.FindLocalBranchCommitAuthor do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          email: binary,
          date: Google.Protobuf.Timestamp.t() | nil,
          timezone: binary
        }

  defstruct name: "",
            email: "",
            date: nil,
            timezone: ""

  field(:name, 1, type: :bytes)
  field(:email, 2, type: :bytes)
  field(:date, 3, type: Google.Protobuf.Timestamp)
  field(:timezone, 4, type: :bytes)
end

defmodule Gitaly.FindAllBranchesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          merged_only: boolean,
          merged_branches: [binary]
        }

  defstruct repository: nil,
            merged_only: false,
            merged_branches: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:merged_only, 2, type: :bool, json_name: "mergedOnly")
  field(:merged_branches, 3, repeated: true, type: :bytes, json_name: "mergedBranches")
end

defmodule Gitaly.FindAllBranchesResponse.Branch do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          target: Gitaly.GitCommit.t() | nil
        }

  defstruct name: "",
            target: nil

  field(:name, 1, type: :bytes)
  field(:target, 2, type: Gitaly.GitCommit)
end

defmodule Gitaly.FindAllBranchesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branches: [Gitaly.FindAllBranchesResponse.Branch.t()]
        }

  defstruct branches: []

  field(:branches, 1, repeated: true, type: Gitaly.FindAllBranchesResponse.Branch)
end

defmodule Gitaly.FindTagRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          tag_name: binary
        }

  defstruct repository: nil,
            tag_name: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:tag_name, 2, type: :bytes, json_name: "tagName")
end

defmodule Gitaly.FindTagResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tag: Gitaly.Tag.t() | nil
        }

  defstruct tag: nil

  field(:tag, 1, type: Gitaly.Tag)
end

defmodule Gitaly.FindAllTagsRequest.SortBy do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          key: Gitaly.FindAllTagsRequest.SortBy.Key.t(),
          direction: Gitaly.SortDirection.t()
        }

  defstruct key: :REFNAME,
            direction: :ASCENDING

  field(:key, 1, type: Gitaly.FindAllTagsRequest.SortBy.Key, enum: true)
  field(:direction, 2, type: Gitaly.SortDirection, enum: true)
end

defmodule Gitaly.FindAllTagsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          sort_by: Gitaly.FindAllTagsRequest.SortBy.t() | nil,
          pagination_params: Gitaly.PaginationParameter.t() | nil
        }

  defstruct repository: nil,
            sort_by: nil,
            pagination_params: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:sort_by, 2, type: Gitaly.FindAllTagsRequest.SortBy, json_name: "sortBy")
  field(:pagination_params, 3, type: Gitaly.PaginationParameter, json_name: "paginationParams")
end

defmodule Gitaly.FindAllTagsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tags: [Gitaly.Tag.t()]
        }

  defstruct tags: []

  field(:tags, 1, repeated: true, type: Gitaly.Tag)
end

defmodule Gitaly.RefExistsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          ref: binary
        }

  defstruct repository: nil,
            ref: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:ref, 2, type: :bytes)
end

defmodule Gitaly.RefExistsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          value: boolean
        }

  defstruct value: false

  field(:value, 1, type: :bool)
end

defmodule Gitaly.CreateBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          name: binary,
          start_point: binary
        }

  defstruct repository: nil,
            name: "",
            start_point: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:name, 2, type: :bytes)
  field(:start_point, 3, type: :bytes, json_name: "startPoint")
end

defmodule Gitaly.CreateBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: Gitaly.CreateBranchResponse.Status.t(),
          branch: Gitaly.Branch.t() | nil
        }

  defstruct status: :OK,
            branch: nil

  field(:status, 1, type: Gitaly.CreateBranchResponse.Status, enum: true)
  field(:branch, 2, type: Gitaly.Branch)
end

defmodule Gitaly.DeleteBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          name: binary
        }

  defstruct repository: nil,
            name: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:name, 2, type: :bytes)
end

defmodule Gitaly.DeleteBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.FindBranchRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          name: binary
        }

  defstruct repository: nil,
            name: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:name, 2, type: :bytes)
end

defmodule Gitaly.FindBranchResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch: Gitaly.Branch.t() | nil
        }

  defstruct branch: nil

  field(:branch, 1, type: Gitaly.Branch)
end

defmodule Gitaly.DeleteRefsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          except_with_prefix: [binary],
          refs: [binary]
        }

  defstruct repository: nil,
            except_with_prefix: [],
            refs: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:except_with_prefix, 2, repeated: true, type: :bytes, json_name: "exceptWithPrefix")
  field(:refs, 3, repeated: true, type: :bytes)
end

defmodule Gitaly.DeleteRefsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          git_error: String.t()
        }

  defstruct git_error: ""

  field(:git_error, 1, type: :string, json_name: "gitError")
end

defmodule Gitaly.ListBranchNamesContainingCommitRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_id: String.t(),
          limit: non_neg_integer
        }

  defstruct repository: nil,
            commit_id: "",
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_id, 2, type: :string, json_name: "commitId")
  field(:limit, 3, type: :uint32)
end

defmodule Gitaly.ListBranchNamesContainingCommitResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branch_names: [binary]
        }

  defstruct branch_names: []

  field(:branch_names, 2, repeated: true, type: :bytes, json_name: "branchNames")
end

defmodule Gitaly.ListTagNamesContainingCommitRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_id: String.t(),
          limit: non_neg_integer
        }

  defstruct repository: nil,
            commit_id: "",
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_id, 2, type: :string, json_name: "commitId")
  field(:limit, 3, type: :uint32)
end

defmodule Gitaly.ListTagNamesContainingCommitResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tag_names: [binary]
        }

  defstruct tag_names: []

  field(:tag_names, 2, repeated: true, type: :bytes, json_name: "tagNames")
end

defmodule Gitaly.GetTagSignaturesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          tag_revisions: [String.t()]
        }

  defstruct repository: nil,
            tag_revisions: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:tag_revisions, 2, repeated: true, type: :string, json_name: "tagRevisions")
end

defmodule Gitaly.GetTagSignaturesResponse.TagSignature do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          tag_id: String.t(),
          signature: binary,
          content: binary
        }

  defstruct tag_id: "",
            signature: "",
            content: ""

  field(:tag_id, 1, type: :string, json_name: "tagId")
  field(:signature, 2, type: :bytes)
  field(:content, 3, type: :bytes)
end

defmodule Gitaly.GetTagSignaturesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          signatures: [Gitaly.GetTagSignaturesResponse.TagSignature.t()]
        }

  defstruct signatures: []

  field(:signatures, 1, repeated: true, type: Gitaly.GetTagSignaturesResponse.TagSignature)
end

defmodule Gitaly.GetTagMessagesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          tag_ids: [String.t()]
        }

  defstruct repository: nil,
            tag_ids: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:tag_ids, 3, repeated: true, type: :string, json_name: "tagIds")
end

defmodule Gitaly.GetTagMessagesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          message: binary,
          tag_id: String.t()
        }

  defstruct message: "",
            tag_id: ""

  field(:message, 2, type: :bytes)
  field(:tag_id, 3, type: :string, json_name: "tagId")
end

defmodule Gitaly.FindAllRemoteBranchesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          remote_name: String.t()
        }

  defstruct repository: nil,
            remote_name: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:remote_name, 2, type: :string, json_name: "remoteName")
end

defmodule Gitaly.FindAllRemoteBranchesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          branches: [Gitaly.Branch.t()]
        }

  defstruct branches: []

  field(:branches, 1, repeated: true, type: Gitaly.Branch)
end

defmodule Gitaly.PackRefsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          all_refs: boolean
        }

  defstruct repository: nil,
            all_refs: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:all_refs, 2, type: :bool, json_name: "allRefs")
end

defmodule Gitaly.PackRefsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Gitaly.ListRefsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          patterns: [binary],
          head: boolean
        }

  defstruct repository: nil,
            patterns: [],
            head: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:patterns, 2, repeated: true, type: :bytes)
  field(:head, 3, type: :bool)
end

defmodule Gitaly.ListRefsResponse.Reference do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: binary,
          target: String.t()
        }

  defstruct name: "",
            target: ""

  field(:name, 1, type: :bytes)
  field(:target, 2, type: :string)
end

defmodule Gitaly.ListRefsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          references: [Gitaly.ListRefsResponse.Reference.t()]
        }

  defstruct references: []

  field(:references, 1, repeated: true, type: Gitaly.ListRefsResponse.Reference)
end

defmodule Gitaly.FindRefsByOIDRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          oid: String.t(),
          ref_patterns: [String.t()],
          sort_field: String.t(),
          limit: non_neg_integer
        }

  defstruct repository: nil,
            oid: "",
            ref_patterns: [],
            sort_field: "",
            limit: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:oid, 2, type: :string)
  field(:ref_patterns, 3, repeated: true, type: :string, json_name: "refPatterns")
  field(:sort_field, 4, type: :string, json_name: "sortField")
  field(:limit, 5, type: :uint32)
end

defmodule Gitaly.FindRefsByOIDResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          refs: [String.t()]
        }

  defstruct refs: []

  field(:refs, 1, repeated: true, type: :string)
end

defmodule Gitaly.RefService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.RefService"

  rpc(
    :FindDefaultBranchName,
    Gitaly.FindDefaultBranchNameRequest,
    Gitaly.FindDefaultBranchNameResponse
  )

  rpc(
    :FindAllBranchNames,
    Gitaly.FindAllBranchNamesRequest,
    stream(Gitaly.FindAllBranchNamesResponse)
  )

  rpc(:FindAllTagNames, Gitaly.FindAllTagNamesRequest, stream(Gitaly.FindAllTagNamesResponse))

  rpc(
    :FindLocalBranches,
    Gitaly.FindLocalBranchesRequest,
    stream(Gitaly.FindLocalBranchesResponse)
  )

  rpc(:FindAllBranches, Gitaly.FindAllBranchesRequest, stream(Gitaly.FindAllBranchesResponse))

  rpc(:FindAllTags, Gitaly.FindAllTagsRequest, stream(Gitaly.FindAllTagsResponse))

  rpc(:FindTag, Gitaly.FindTagRequest, Gitaly.FindTagResponse)

  rpc(
    :FindAllRemoteBranches,
    Gitaly.FindAllRemoteBranchesRequest,
    stream(Gitaly.FindAllRemoteBranchesResponse)
  )

  rpc(:RefExists, Gitaly.RefExistsRequest, Gitaly.RefExistsResponse)

  rpc(:FindBranch, Gitaly.FindBranchRequest, Gitaly.FindBranchResponse)

  rpc(:DeleteRefs, Gitaly.DeleteRefsRequest, Gitaly.DeleteRefsResponse)

  rpc(
    :ListBranchNamesContainingCommit,
    Gitaly.ListBranchNamesContainingCommitRequest,
    stream(Gitaly.ListBranchNamesContainingCommitResponse)
  )

  rpc(
    :ListTagNamesContainingCommit,
    Gitaly.ListTagNamesContainingCommitRequest,
    stream(Gitaly.ListTagNamesContainingCommitResponse)
  )

  rpc(:GetTagSignatures, Gitaly.GetTagSignaturesRequest, stream(Gitaly.GetTagSignaturesResponse))

  rpc(:GetTagMessages, Gitaly.GetTagMessagesRequest, stream(Gitaly.GetTagMessagesResponse))

  rpc(:PackRefs, Gitaly.PackRefsRequest, Gitaly.PackRefsResponse)

  rpc(:ListRefs, Gitaly.ListRefsRequest, stream(Gitaly.ListRefsResponse))

  rpc(:FindRefsByOID, Gitaly.FindRefsByOIDRequest, Gitaly.FindRefsByOIDResponse)
end

defmodule Gitaly.RefService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.RefService.Service
end
