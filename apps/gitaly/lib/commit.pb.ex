defmodule Gitaly.ListCommitsRequest.Order do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :TOPO | :DATE

  field(:NONE, 0)
  field(:TOPO, 1)
  field(:DATE, 2)
end

defmodule Gitaly.TreeEntryResponse.ObjectType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :COMMIT | :BLOB | :TREE | :TAG

  field(:COMMIT, 0)
  field(:BLOB, 1)
  field(:TREE, 2)
  field(:TAG, 3)
end

defmodule Gitaly.TreeEntry.EntryType do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :BLOB | :TREE | :COMMIT

  field(:BLOB, 0)
  field(:TREE, 1)
  field(:COMMIT, 3)
end

defmodule Gitaly.GetTreeEntriesRequest.SortBy do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :DEFAULT | :TREES_FIRST

  field(:DEFAULT, 0)
  field(:TREES_FIRST, 1)
end

defmodule Gitaly.FindAllCommitsRequest.Order do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :TOPO | :DATE

  field(:NONE, 0)
  field(:TOPO, 1)
  field(:DATE, 2)
end

defmodule Gitaly.FindCommitsRequest.Order do
  @moduledoc false
  use Protobuf, enum: true, syntax: :proto3

  @type t :: integer | :NONE | :TOPO

  field(:NONE, 0)
  field(:TOPO, 1)
end

defmodule Gitaly.ListCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revisions: [String.t()],
          pagination_params: Gitaly.PaginationParameter.t() | nil,
          order: Gitaly.ListCommitsRequest.Order.t(),
          reverse: boolean,
          max_parents: non_neg_integer,
          disable_walk: boolean,
          first_parent: boolean,
          after: Google.Protobuf.Timestamp.t() | nil,
          before: Google.Protobuf.Timestamp.t() | nil,
          author: binary
        }

  defstruct repository: nil,
            revisions: [],
            pagination_params: nil,
            order: :NONE,
            reverse: false,
            max_parents: 0,
            disable_walk: false,
            first_parent: false,
            after: nil,
            before: nil,
            author: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revisions, 2, repeated: true, type: :string)
  field(:pagination_params, 3, type: Gitaly.PaginationParameter, json_name: "paginationParams")
  field(:order, 4, type: Gitaly.ListCommitsRequest.Order, enum: true)
  field(:reverse, 11, type: :bool)
  field(:max_parents, 5, type: :uint32, json_name: "maxParents")
  field(:disable_walk, 6, type: :bool, json_name: "disableWalk")
  field(:first_parent, 7, type: :bool, json_name: "firstParent")
  field(:after, 8, type: Google.Protobuf.Timestamp)
  field(:before, 9, type: Google.Protobuf.Timestamp)
  field(:author, 10, type: :bytes)
end

defmodule Gitaly.ListCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.ListAllCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          pagination_params: Gitaly.PaginationParameter.t() | nil
        }

  defstruct repository: nil,
            pagination_params: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:pagination_params, 2, type: Gitaly.PaginationParameter, json_name: "paginationParams")
end

defmodule Gitaly.ListAllCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.CommitStatsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary
        }

  defstruct repository: nil,
            revision: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
end

defmodule Gitaly.CommitStatsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          oid: String.t(),
          additions: integer,
          deletions: integer
        }

  defstruct oid: "",
            additions: 0,
            deletions: 0

  field(:oid, 1, type: :string)
  field(:additions, 2, type: :int32)
  field(:deletions, 3, type: :int32)
end

defmodule Gitaly.CommitIsAncestorRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          ancestor_id: String.t(),
          child_id: String.t()
        }

  defstruct repository: nil,
            ancestor_id: "",
            child_id: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:ancestor_id, 2, type: :string, json_name: "ancestorId")
  field(:child_id, 3, type: :string, json_name: "childId")
end

defmodule Gitaly.CommitIsAncestorResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          value: boolean
        }

  defstruct value: false

  field(:value, 1, type: :bool)
end

defmodule Gitaly.TreeEntryRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          path: binary,
          limit: integer,
          max_size: integer
        }

  defstruct repository: nil,
            revision: "",
            path: "",
            limit: 0,
            max_size: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:path, 3, type: :bytes)
  field(:limit, 4, type: :int64)
  field(:max_size, 5, type: :int64, json_name: "maxSize")
end

defmodule Gitaly.TreeEntryResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          type: Gitaly.TreeEntryResponse.ObjectType.t(),
          oid: String.t(),
          size: integer,
          mode: integer,
          data: binary
        }

  defstruct type: :COMMIT,
            oid: "",
            size: 0,
            mode: 0,
            data: ""

  field(:type, 1, type: Gitaly.TreeEntryResponse.ObjectType, enum: true)
  field(:oid, 2, type: :string)
  field(:size, 3, type: :int64)
  field(:mode, 4, type: :int32)
  field(:data, 5, type: :bytes)
end

defmodule Gitaly.CommitsBetweenRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          from: binary,
          to: binary,
          pagination_params: Gitaly.PaginationParameter.t() | nil
        }

  defstruct repository: nil,
            from: "",
            to: "",
            pagination_params: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:from, 2, type: :bytes)
  field(:to, 3, type: :bytes)
  field(:pagination_params, 4, type: Gitaly.PaginationParameter, json_name: "paginationParams")
end

defmodule Gitaly.CommitsBetweenResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.CountCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          after: Google.Protobuf.Timestamp.t() | nil,
          before: Google.Protobuf.Timestamp.t() | nil,
          path: binary,
          max_count: integer,
          all: boolean,
          first_parent: boolean,
          global_options: Gitaly.GlobalOptions.t() | nil
        }

  defstruct repository: nil,
            revision: "",
            after: nil,
            before: nil,
            path: "",
            max_count: 0,
            all: false,
            first_parent: false,
            global_options: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:after, 3, type: Google.Protobuf.Timestamp)
  field(:before, 4, type: Google.Protobuf.Timestamp)
  field(:path, 5, type: :bytes)
  field(:max_count, 6, type: :int32, json_name: "maxCount")
  field(:all, 7, type: :bool)
  field(:first_parent, 8, type: :bool, json_name: "firstParent")
  field(:global_options, 9, type: Gitaly.GlobalOptions, json_name: "globalOptions")
end

defmodule Gitaly.CountCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          count: integer
        }

  defstruct count: 0

  field(:count, 1, type: :int32)
end

defmodule Gitaly.CountDivergingCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          from: binary,
          to: binary,
          max_count: integer
        }

  defstruct repository: nil,
            from: "",
            to: "",
            max_count: 0

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:from, 2, type: :bytes)
  field(:to, 3, type: :bytes)
  field(:max_count, 7, type: :int32, json_name: "maxCount")
end

defmodule Gitaly.CountDivergingCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          left_count: integer,
          right_count: integer
        }

  defstruct left_count: 0,
            right_count: 0

  field(:left_count, 1, type: :int32, json_name: "leftCount")
  field(:right_count, 2, type: :int32, json_name: "rightCount")
end

defmodule Gitaly.TreeEntry do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          oid: String.t(),
          root_oid: String.t(),
          path: binary,
          type: Gitaly.TreeEntry.EntryType.t(),
          mode: integer,
          commit_oid: String.t(),
          flat_path: binary
        }

  defstruct oid: "",
            root_oid: "",
            path: "",
            type: :BLOB,
            mode: 0,
            commit_oid: "",
            flat_path: ""

  field(:oid, 1, type: :string)
  field(:root_oid, 2, type: :string, json_name: "rootOid")
  field(:path, 3, type: :bytes)
  field(:type, 4, type: Gitaly.TreeEntry.EntryType, enum: true)
  field(:mode, 5, type: :int32)
  field(:commit_oid, 6, type: :string, json_name: "commitOid")
  field(:flat_path, 7, type: :bytes, json_name: "flatPath")
end

defmodule Gitaly.GetTreeEntriesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          path: binary,
          recursive: boolean,
          sort: Gitaly.GetTreeEntriesRequest.SortBy.t(),
          pagination_params: Gitaly.PaginationParameter.t() | nil
        }

  defstruct repository: nil,
            revision: "",
            path: "",
            recursive: false,
            sort: :DEFAULT,
            pagination_params: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:path, 3, type: :bytes)
  field(:recursive, 4, type: :bool)
  field(:sort, 5, type: Gitaly.GetTreeEntriesRequest.SortBy, enum: true)
  field(:pagination_params, 6, type: Gitaly.PaginationParameter, json_name: "paginationParams")
end

defmodule Gitaly.GetTreeEntriesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          entries: [Gitaly.TreeEntry.t()],
          pagination_cursor: Gitaly.PaginationCursor.t() | nil
        }

  defstruct entries: [],
            pagination_cursor: nil

  field(:entries, 1, repeated: true, type: Gitaly.TreeEntry)
  field(:pagination_cursor, 2, type: Gitaly.PaginationCursor, json_name: "paginationCursor")
end

defmodule Gitaly.ListFilesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary
        }

  defstruct repository: nil,
            revision: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
end

defmodule Gitaly.ListFilesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          paths: [binary]
        }

  defstruct paths: []

  field(:paths, 1, repeated: true, type: :bytes)
end

defmodule Gitaly.FindCommitRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          trailers: boolean
        }

  defstruct repository: nil,
            revision: "",
            trailers: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:trailers, 3, type: :bool)
end

defmodule Gitaly.FindCommitResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit: Gitaly.GitCommit.t() | nil
        }

  defstruct commit: nil

  field(:commit, 1, type: Gitaly.GitCommit)
end

defmodule Gitaly.ListCommitsByOidRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          oid: [String.t()]
        }

  defstruct repository: nil,
            oid: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:oid, 2, repeated: true, type: :string)
end

defmodule Gitaly.ListCommitsByOidResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.ListCommitsByRefNameRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          ref_names: [binary]
        }

  defstruct repository: nil,
            ref_names: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:ref_names, 2, repeated: true, type: :bytes, json_name: "refNames")
end

defmodule Gitaly.ListCommitsByRefNameResponse.CommitForRef do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit: Gitaly.GitCommit.t() | nil,
          ref_name: binary
        }

  defstruct commit: nil,
            ref_name: ""

  field(:commit, 1, type: Gitaly.GitCommit)
  field(:ref_name, 2, type: :bytes, json_name: "refName")
end

defmodule Gitaly.ListCommitsByRefNameResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_refs: [Gitaly.ListCommitsByRefNameResponse.CommitForRef.t()]
        }

  defstruct commit_refs: []

  field(:commit_refs, 2,
    repeated: true,
    type: Gitaly.ListCommitsByRefNameResponse.CommitForRef,
    json_name: "commitRefs"
  )
end

defmodule Gitaly.FindAllCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          max_count: integer,
          skip: integer,
          order: Gitaly.FindAllCommitsRequest.Order.t()
        }

  defstruct repository: nil,
            revision: "",
            max_count: 0,
            skip: 0,
            order: :NONE

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:max_count, 3, type: :int32, json_name: "maxCount")
  field(:skip, 4, type: :int32)
  field(:order, 5, type: Gitaly.FindAllCommitsRequest.Order, enum: true)
end

defmodule Gitaly.FindAllCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.FindCommitsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          limit: integer,
          offset: integer,
          paths: [binary],
          follow: boolean,
          skip_merges: boolean,
          disable_walk: boolean,
          after: Google.Protobuf.Timestamp.t() | nil,
          before: Google.Protobuf.Timestamp.t() | nil,
          all: boolean,
          first_parent: boolean,
          author: binary,
          order: Gitaly.FindCommitsRequest.Order.t(),
          global_options: Gitaly.GlobalOptions.t() | nil,
          trailers: boolean
        }

  defstruct repository: nil,
            revision: "",
            limit: 0,
            offset: 0,
            paths: [],
            follow: false,
            skip_merges: false,
            disable_walk: false,
            after: nil,
            before: nil,
            all: false,
            first_parent: false,
            author: "",
            order: :NONE,
            global_options: nil,
            trailers: false

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:limit, 3, type: :int32)
  field(:offset, 4, type: :int32)
  field(:paths, 5, repeated: true, type: :bytes)
  field(:follow, 6, type: :bool)
  field(:skip_merges, 7, type: :bool, json_name: "skipMerges")
  field(:disable_walk, 8, type: :bool, json_name: "disableWalk")
  field(:after, 9, type: Google.Protobuf.Timestamp)
  field(:before, 10, type: Google.Protobuf.Timestamp)
  field(:all, 11, type: :bool)
  field(:first_parent, 12, type: :bool, json_name: "firstParent")
  field(:author, 13, type: :bytes)
  field(:order, 14, type: Gitaly.FindCommitsRequest.Order, enum: true)
  field(:global_options, 15, type: Gitaly.GlobalOptions, json_name: "globalOptions")
  field(:trailers, 16, type: :bool)
end

defmodule Gitaly.FindCommitsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.CommitLanguagesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary
        }

  defstruct repository: nil,
            revision: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
end

defmodule Gitaly.CommitLanguagesResponse.Language do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          name: String.t(),
          share: float | :infinity | :negative_infinity | :nan,
          color: String.t(),
          file_count: non_neg_integer,
          bytes: non_neg_integer
        }

  defstruct name: "",
            share: 0.0,
            color: "",
            file_count: 0,
            bytes: 0

  field(:name, 1, type: :string)
  field(:share, 2, type: :float)
  field(:color, 3, type: :string)
  field(:file_count, 4, type: :uint32, json_name: "fileCount")
  field(:bytes, 5, type: :uint64)
end

defmodule Gitaly.CommitLanguagesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          languages: [Gitaly.CommitLanguagesResponse.Language.t()]
        }

  defstruct languages: []

  field(:languages, 1, repeated: true, type: Gitaly.CommitLanguagesResponse.Language)
end

defmodule Gitaly.RawBlameRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          path: binary
        }

  defstruct repository: nil,
            revision: "",
            path: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:path, 3, type: :bytes)
end

defmodule Gitaly.RawBlameResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          data: binary
        }

  defstruct data: ""

  field(:data, 1, type: :bytes)
end

defmodule Gitaly.LastCommitForPathRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          path: binary,
          literal_pathspec: boolean,
          global_options: Gitaly.GlobalOptions.t() | nil
        }

  defstruct repository: nil,
            revision: "",
            path: "",
            literal_pathspec: false,
            global_options: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:path, 3, type: :bytes)
  field(:literal_pathspec, 4, type: :bool, json_name: "literalPathspec")
  field(:global_options, 5, type: Gitaly.GlobalOptions, json_name: "globalOptions")
end

defmodule Gitaly.LastCommitForPathResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit: Gitaly.GitCommit.t() | nil
        }

  defstruct commit: nil

  field(:commit, 1, type: Gitaly.GitCommit)
end

defmodule Gitaly.ListLastCommitsForTreeRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: String.t(),
          path: binary,
          limit: integer,
          offset: integer,
          literal_pathspec: boolean,
          global_options: Gitaly.GlobalOptions.t() | nil
        }

  defstruct repository: nil,
            revision: "",
            path: "",
            limit: 0,
            offset: 0,
            literal_pathspec: false,
            global_options: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :string)
  field(:path, 3, type: :bytes)
  field(:limit, 4, type: :int32)
  field(:offset, 5, type: :int32)
  field(:literal_pathspec, 6, type: :bool, json_name: "literalPathspec", deprecated: true)
  field(:global_options, 7, type: Gitaly.GlobalOptions, json_name: "globalOptions")
end

defmodule Gitaly.ListLastCommitsForTreeResponse.CommitForTree do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit: Gitaly.GitCommit.t() | nil,
          path_bytes: binary
        }

  defstruct commit: nil,
            path_bytes: ""

  field(:commit, 2, type: Gitaly.GitCommit)
  field(:path_bytes, 4, type: :bytes, json_name: "pathBytes")
end

defmodule Gitaly.ListLastCommitsForTreeResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.ListLastCommitsForTreeResponse.CommitForTree.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.ListLastCommitsForTreeResponse.CommitForTree)
end

defmodule Gitaly.CommitsByMessageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          revision: binary,
          offset: integer,
          limit: integer,
          path: binary,
          query: String.t(),
          global_options: Gitaly.GlobalOptions.t() | nil
        }

  defstruct repository: nil,
            revision: "",
            offset: 0,
            limit: 0,
            path: "",
            query: "",
            global_options: nil

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:revision, 2, type: :bytes)
  field(:offset, 3, type: :int32)
  field(:limit, 4, type: :int32)
  field(:path, 5, type: :bytes)
  field(:query, 6, type: :string)
  field(:global_options, 7, type: Gitaly.GlobalOptions, json_name: "globalOptions")
end

defmodule Gitaly.CommitsByMessageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commits: [Gitaly.GitCommit.t()]
        }

  defstruct commits: []

  field(:commits, 1, repeated: true, type: Gitaly.GitCommit)
end

defmodule Gitaly.FilterShasWithSignaturesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          shas: [binary]
        }

  defstruct repository: nil,
            shas: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:shas, 2, repeated: true, type: :bytes)
end

defmodule Gitaly.FilterShasWithSignaturesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          shas: [binary]
        }

  defstruct shas: []

  field(:shas, 1, repeated: true, type: :bytes)
end

defmodule Gitaly.ExtractCommitSignatureRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_id: String.t()
        }

  defstruct repository: nil,
            commit_id: ""

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_id, 2, type: :string, json_name: "commitId")
end

defmodule Gitaly.ExtractCommitSignatureResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          signature: binary,
          signed_text: binary
        }

  defstruct signature: "",
            signed_text: ""

  field(:signature, 1, type: :bytes)
  field(:signed_text, 2, type: :bytes, json_name: "signedText")
end

defmodule Gitaly.GetCommitSignaturesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_ids: [String.t()]
        }

  defstruct repository: nil,
            commit_ids: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_ids, 2, repeated: true, type: :string, json_name: "commitIds")
end

defmodule Gitaly.GetCommitSignaturesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_id: String.t(),
          signature: binary,
          signed_text: binary
        }

  defstruct commit_id: "",
            signature: "",
            signed_text: ""

  field(:commit_id, 1, type: :string, json_name: "commitId")
  field(:signature, 2, type: :bytes)
  field(:signed_text, 3, type: :bytes, json_name: "signedText")
end

defmodule Gitaly.GetCommitMessagesRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          repository: Gitaly.Repository.t() | nil,
          commit_ids: [String.t()]
        }

  defstruct repository: nil,
            commit_ids: []

  field(:repository, 1, type: Gitaly.Repository, deprecated: false)
  field(:commit_ids, 2, repeated: true, type: :string, json_name: "commitIds")
end

defmodule Gitaly.GetCommitMessagesResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commit_id: String.t(),
          message: binary
        }

  defstruct commit_id: "",
            message: ""

  field(:commit_id, 1, type: :string, json_name: "commitId")
  field(:message, 2, type: :bytes)
end

defmodule Gitaly.CommitService.Service do
  @moduledoc false
  use GRPC.Service, name: "gitaly.CommitService"

  rpc(:ListCommits, Gitaly.ListCommitsRequest, stream(Gitaly.ListCommitsResponse))

  rpc(:ListAllCommits, Gitaly.ListAllCommitsRequest, stream(Gitaly.ListAllCommitsResponse))

  rpc(:CommitIsAncestor, Gitaly.CommitIsAncestorRequest, Gitaly.CommitIsAncestorResponse)

  rpc(:TreeEntry, Gitaly.TreeEntryRequest, stream(Gitaly.TreeEntryResponse))

  rpc(:CommitsBetween, Gitaly.CommitsBetweenRequest, stream(Gitaly.CommitsBetweenResponse))

  rpc(:CountCommits, Gitaly.CountCommitsRequest, Gitaly.CountCommitsResponse)

  rpc(
    :CountDivergingCommits,
    Gitaly.CountDivergingCommitsRequest,
    Gitaly.CountDivergingCommitsResponse
  )

  rpc(:GetTreeEntries, Gitaly.GetTreeEntriesRequest, stream(Gitaly.GetTreeEntriesResponse))

  rpc(:ListFiles, Gitaly.ListFilesRequest, stream(Gitaly.ListFilesResponse))

  rpc(:FindCommit, Gitaly.FindCommitRequest, Gitaly.FindCommitResponse)

  rpc(:CommitStats, Gitaly.CommitStatsRequest, Gitaly.CommitStatsResponse)

  rpc(:FindAllCommits, Gitaly.FindAllCommitsRequest, stream(Gitaly.FindAllCommitsResponse))

  rpc(:FindCommits, Gitaly.FindCommitsRequest, stream(Gitaly.FindCommitsResponse))

  rpc(:CommitLanguages, Gitaly.CommitLanguagesRequest, Gitaly.CommitLanguagesResponse)

  rpc(:RawBlame, Gitaly.RawBlameRequest, stream(Gitaly.RawBlameResponse))

  rpc(:LastCommitForPath, Gitaly.LastCommitForPathRequest, Gitaly.LastCommitForPathResponse)

  rpc(
    :ListLastCommitsForTree,
    Gitaly.ListLastCommitsForTreeRequest,
    stream(Gitaly.ListLastCommitsForTreeResponse)
  )

  rpc(:CommitsByMessage, Gitaly.CommitsByMessageRequest, stream(Gitaly.CommitsByMessageResponse))

  rpc(:ListCommitsByOid, Gitaly.ListCommitsByOidRequest, stream(Gitaly.ListCommitsByOidResponse))

  rpc(
    :ListCommitsByRefName,
    Gitaly.ListCommitsByRefNameRequest,
    stream(Gitaly.ListCommitsByRefNameResponse)
  )

  rpc(
    :FilterShasWithSignatures,
    stream(Gitaly.FilterShasWithSignaturesRequest),
    stream(Gitaly.FilterShasWithSignaturesResponse)
  )

  rpc(
    :GetCommitSignatures,
    Gitaly.GetCommitSignaturesRequest,
    stream(Gitaly.GetCommitSignaturesResponse)
  )

  rpc(
    :GetCommitMessages,
    Gitaly.GetCommitMessagesRequest,
    stream(Gitaly.GetCommitMessagesResponse)
  )
end

defmodule Gitaly.CommitService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Gitaly.CommitService.Service
end
