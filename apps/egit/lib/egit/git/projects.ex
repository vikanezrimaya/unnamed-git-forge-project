defmodule EGit.Git.Projects do
  @moduledoc """
  The Project context.
  """

  import Ecto.Query, warn: false
  alias EGit.Repo

  alias EGit.Accounts.User
  alias EGit.Git.Project
  alias EGit.Git.Project.{Features}
  alias EGit.Git.Projects.Gitaly

  @doc """
  Get a single project

  ## Examples
  iex> get_project(1)
  %Project{}

  iex> get_project(456)
  nil
  """
  def get_project(id) when is_integer(id) do
    Repo.get_by(Project, id: id)
  end

  @doc """
  Get a single project

  Raises `Ecto.NoResultsError` if the Project does not exist.

  ## Examples
  iex> get_project(1)
  %Project{}

  iex> get_project(456)
  ** (Ecto.NoResultsError)
  """
  def get_project!(id) when is_integer(id) do
    Repo.get_by!(Project, id: id)
  end

  @doc """
  Create a project for the given user with the uid as path

  attrs can hold project infos and project features

  ## Examples
  iex> create_project(%User{id: 1, user_id: "user"}, "repo")
  """
  def create_project(%User{} = user, name, attrs \\ %{}) when is_binary(name) do
    create_project_at_path(user, user.uid, name, attrs)
  end

  @doc """
  Create a project for the given user with a path

  attrs can hold project infos and project features

  ## Attrs
  Can hold:
  - `description`
  - `archived`
  - `project_access_level`

  ## Examples
  iex> create_project(%User{id: 1, user_id: "user"}, "repo")
  {:ok, %Project{}}
  """
  # FIXME: should this create an atuhorization entry for the owner with owner access rights
  def create_project_at_path(%User{} = user, path, name, attrs \\ %{})
      when is_binary(name) and is_binary(path) do
    rel_path = Gitaly.get_project_path(path, name)

    case create_project(user.id, name, rel_path, attrs) do
      {:ok, project} ->
        Gitaly.new_gitaly_project(project)

      {:error, changeset} ->
        {:error, {:ecto, changeset}}
    end
  end

  def remove_project(%Project{} = project) do
    Gitaly.delete_gitaly_project(project)
    |> case do
      {:ok} ->
        Repo.delete(project)
        |> case do
          {:ok, v} -> {:ok, v}
          {:error, e} -> {:error, {:ecto, e}}
        end

      v ->
        v
    end

    # TODO: remove project from database, and then from gitaly
  end

  defp create_project(creator_id, name, path, attrs) when is_integer(creator_id) do
    int_attrs = %{creator_id: creator_id, name: name, path: path}

    %Project{}
    |> Project.create_changeset_with_path(int_attrs, attrs)
    |> Repo.insert()
    |> case do
      {:ok, project} ->
        case create_features(project, attrs) do
          {:ok, _features} ->
            {:ok, project}

          {:error, err} ->
            {:error, {:features, err}}
        end

      {:error, err} ->
        {:error, {:ecto, err}}
    end

    # TODO: create features
  end

  defp create_features(%Project{} = project, attrs) do
    %Features{project_id: project.id}
    |> Features.create_changeset(attrs)
    |> Repo.insert()
  end
end
