defmodule EGit.Git.Projects.Gitaly do
  alias EGit.Git.Project

  def project_to_repo(%Project{} = project, opts \\ []) do
    storage_name = Keyword.get(opts, :storage_name, get_project_storage)
    %Gitaly.Repository{storage_name: storage_name, relative_path: project.path}
  end

  def new_gitaly_project(%Project{} = project) do
    repo = project_to_repo(project)
    req = Gitaly.CreateRepositoryRequest.new(repository: repo)
    rpc = get_rpc()

    case Gitaly.RepositoryService.Stub.create_repository(rpc, req) do
      {:ok, %Gitaly.CreateRepositoryResponse{}} ->
        {:ok, project}

      {:error, e} ->
        # FIXME: remove repo from db
        {:error, {:gitaly, e}}
    end
  end

  def delete_gitaly_project(%Project{} = project) do
    project
    |> project_to_repo
    |> delete_gitaly_project
  end

  def delete_gitaly_project(%Gitaly.Repository{} = repo) do
    req = Gitaly.RemoveRepositoryRequest.new(repository: repo)

    get_rpc()
    |> Gitaly.RepositoryService.Stub.remove_repository(req)
    |> case do
      {:ok, %Gitaly.RemoveRepositoryResponse{}} ->
        {:ok}

      {:error, e} ->
        {:error, {:gitaly, e}}
    end
  end

  # TODO: make it configurable for repo groups
  def get_project_storage, do: "egit"
  # TODO: hash it?
  def get_project_path(path, name), do: Path.join("@path", Path.join(path, name))

  defp get_rpc(), do: EGit.Git.RPC.get_rpc(:repo)
end

# def new(userid, path, name) when is_binary(path) and is_binary(name) do
#  rel_path =
#    get_path(path, name)
#    |> IO.inspect()

#  # TODO: uniqueness via ecto db

#  storage = get_storage()
#  repo = %Gitaly.Repository{storage_name: storage, relative_path: rel_path}
#  req = Gitaly.CreateRepositoryRequest.new(repository: repo)
#  rpc = get_rpc()

#  Gitaly.RepositoryService.Stub.create_repository(rpc, req)
# end
