defmodule EGit.Git.Project.Features do
  use Ecto.Schema
  import Ecto.Changeset

  schema "project_features" do
    belongs_to :project, EGit.Git.Project
    field :project_access_level, :integer

    timestamps()
  end

  def create_changeset(features, attrs) do
    features
    |> cast(attrs, [:project_access_level])
    |> validate_required([:project_id])
  end

  # TODO: name mapping for project_access_level?
end
