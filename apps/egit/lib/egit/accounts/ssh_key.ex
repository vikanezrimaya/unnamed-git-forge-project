defmodule EGit.Accounts.SshKey do
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  schema "user_ssh_keys" do
    # migration 20211204172013
    field :name, :string
    field :key, :binary
    field :type, :string, virtual: true
    field :fingerprint_md5, :string, virtual: true
    field :fingerprint_sha, :string, virtual: true
    field :last_used, :utc_datetime
    belongs_to :user, EGit.Accounts.User

    timestamps()
  end

  def user_id_query(%EGit.Accounts.User{} = user, id) do
    from t in EGit.Accounts.SshKey,
      where: t.user_id == ^user.id and t.id == ^id
  end

  def user_query(%EGit.Accounts.User{} = user) do
    from t in EGit.Accounts.SshKey, where: t.user_id == ^user.id
  end

  def user_query(%EGit.Accounts.User{} = user, limit, offset \\ 0) do
    user_query(user)
    |> limit(^limit)
    |> offset(^offset)
  end

  def fill_virtual(key) when key != nil do
    case parse_key(key.key) do
      {:ok, {type, _key, {finger_md, finger_sha}, _comment}} ->
        key
        |> Map.put(:fingerprint_md5, finger_md)
        |> Map.put(:fingerprint_sha, finger_sha)
        |> Map.put(:type, type)

      _ ->
        key
    end
  end

  def fill_virtual(key), do: key

  def create_changeset(key, attrs) do
    key
    |> cast(attrs, [:name, :key])
    |> validate_name()
  end

  def create_changeset_key(key, attrs) do
    key
    |> cast(attrs, [:name, :key])
    # key has to be valiadet beforve name, so not reusing
    |> validate_key()
    |> validate_name()
  end

  defp validate_name(changeset) do
    changeset
    |> validate_required([:name])
    # TODO: format?
    |> validate_length(:name, min: 3, max: 50)
  end

  defp validate_key(changeset) do
    key = get_change(changeset, :key)

    changeset =
      changeset
      |> validate_required([:key])

    if key do
      changeset
      |> validate_key(parse_key(key))
    else
      changeset
    end
  end

  defp validate_key(changeset, {:ok, key}) do
    {type, _key, {finger_md, finger_sha}, comment} = key

    changeset
    |> add_comment(comment)
    # TODO: is this a good idea?
    |> put_change(:type, type)
    |> put_change(:fingerprint_md5, finger_md)
    |> put_change(:fingerprint_sha, finger_sha)
  end

  defp validate_key(changeset, {:error, t}) do
    changeset
    |> add_error(:key, "Could not parse key", error: t)
  end

  defp add_comment(changeset, comment) when is_list(comment) do
    has_name? = get_change(changeset, :name) != nil

    if !has_name? do
      changeset
      |> put_change(:name, :binary.list_to_bin(comment))
    else
      changeset
    end
  end

  # FIXME: sanitize key bevore decoding. erlang crashes on invalid base64
  def parse_key(key) when is_binary(key) do
    try do
      :ssh_file.decode(key, :public_key)
      |> parse_key
    rescue
      _ ->
        {:error, :failed_to_parse_key}
    end
  end

  def parse_key([key]) do
    {key, opts} = key
    comment = Keyword.get(opts, :comment)

    type = get_type(key)
    [finger_md, finger_sha] = :ssh.hostkey_fingerprint([:md5, :sha256], key)

    finger_md =
      finger_md
      |> :binary.list_to_bin()

    finger_sha =
      finger_sha
      |> :binary.list_to_bin()

    {:ok, {type, key, {finger_md, finger_sha}, comment}}
  end

  def parse_key([]) do
    {:error, "could not parse key"}
  end

  defp get_type({:ed_pub, t, _data}) do
    t
  end

  defp get_type({:RSAPublicKey, _, _}) do
    :rsa
  end

  defp get_type({t, _data}) do
    t
  end

  defp get_type(_) do
    {:error, :unknown_type}
  end
end
