defmodule EGit.Accounts.Indieauth do
  alias EGit.Accounts.Indieauth.Client
  @moduledoc false

  # TODO: verify not hitting a 404
  def get_auth_endpoint(domain, client_id, redirect_uri, extra_args \\ %{}) do
    domain = if(String.starts_with?(domain, "http"), do: domain, else: "https://" <> domain)

    get_auth_base_endpoint(domain)
    |> get_auth_endpoint(domain, client_id, redirect_uri, extra_args)
  end

  def get_auth_endpoint({:ok, auth_domain}, domain, client_id, redirect_uri, extra_args) do
    auth_params =
      %{
        "me" => domain,
        "client_id" => client_id,
        "redirect_uri" => redirect_uri,
        "response_type" => "code"
      }
      |> Map.merge(extra_args)

    auth_domain =
      auth_domain
      |> URI.parse()
      |> Map.put(:query, URI.encode_query(auth_params))
      |> URI.to_string()

    {:ok, auth_domain}
  end

  def get_auth_endpoint({:error, v}, _domain, _client_id, _redirect_uri, _extra_args) do
    {:error, v}
  end

  def get_auth_base_endpoint(domain) do
    Client.get_url(domain)
    |> parse_auth_endpoint
  end

  def verify(code, me, client_id, redirect_uri) do
    verify(code, me, client_id, redirect_uri, get_auth_base_endpoint(me))
  end

  def verify(code, _me, client_id, redirect_uri, {:ok, base_endpoint}) do
    body =
      %{
        "code" => code,
        "redirect_uri" => redirect_uri,
        "client_id" => client_id
      }
      |> URI.encode_query()

    Client.post_url(base_endpoint, body)
    |> verify
  end

  def verify(_code, _me, _client_id, _redirect_uri, {:error, err}) do
    {:error, err}
  end

  def verify({:ok, tesla}) do
    case tesla.status do
      200 -> find_me(tesla.body)
      _ -> {:error, :non_success_status}
    end
  end

  def verify({:error, err}) do
    {:error, err}
  end

  defp parse_auth_endpoint({:ok, tesla}) do
    tesla.body
    |> Microformats2.parse(tesla.body)
    |> Map.get("rels", %{})
    |> Map.get("authorization_endpoint", [])
    |> parse_auth_endpoint
  end

  defp parse_auth_endpoint({:error, v}) do
    {:error, v}
  end

  defp parse_auth_endpoint([authorization_endpoint]) do
    {:ok, authorization_endpoint}
  end

  defp parse_auth_endpoint([]) do
    {:error, :no_authorization_endpoint}
  end

  defp find_me(body) when is_binary(body) do
    me =
      URI.decode_query(body)
      |> Map.get("me")

    if is_binary(me) do
      {:ok, me}
    else
      {:error, :invalid_code}
    end
  end
end
