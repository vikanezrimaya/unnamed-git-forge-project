defmodule EGit.Repo.Migrations.CreateSshKeys do
  use Ecto.Migration

  def change do
    create table(:user_ssh_keys) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :name, :string, null: false
      add :key, :binary, null: false
      add :last_used, :utc_datetime

      timestamps()
    end

    create index(:user_ssh_keys, [:user_id])
  end
end
