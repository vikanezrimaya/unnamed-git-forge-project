defmodule EGitWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      EGitWeb.Telemetry,
      # Start the Endpoint (http/https)
      EGitWeb.Endpoint
      # Start a worker by calling: EGitWeb.Worker.start_link(arg)
      # {EGitWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: EGitWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    EGitWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
