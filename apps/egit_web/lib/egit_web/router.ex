defmodule EGitWeb.Router do
  use EGitWeb, :router

  import EGitWeb.UserAuth
  import EGitWeb.UserSettings

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {EGitWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :settings do
    plug :current_settings_page
    plug :put_layout, {EGitWeb.LayoutView, :settings}
  end

  scope "/", EGitWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", EGitWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: EGitWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Project routes
  scope "/projects", EGitWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/new", ProjectNewController, :index
    post "/new", ProjectNewController, :create
  end

  ## Authentication routes
  scope "/users/" do
    # register, login, reset_password
    scope "/", EGitWeb do
      pipe_through [:browser, :redirect_if_user_is_authenticated]

      get "/register", UserRegistrationController, :new
      post "/register", UserRegistrationController, :create
      get "/log_in", UserSessionController, :new
      post "/log_in", UserSessionController, :create
      get "/reset_password", UserResetPasswordController, :new
      post "/reset_password", UserResetPasswordController, :create
      get "/reset_password/:token", UserresetPasswordController, :edit
      put "/reset_password/:token", UserResetPasswordController, :update
    end

    # indieauth
    scope "/indieauth", EGitWeb do
      pipe_through [:browser, :redirect_if_user_is_authenticated]

      get "/", UserIndieauthController, :new
      post "/", UserIndieauthController, :create
      get "/callback", UserIndieauthController, :callback
      post "/update", UserIndieauthController, :update
    end

    # settings
    scope "/settings", EGitWeb do
      pipe_through [:browser, :require_authenticated_user, :settings]

      get "/", UserSettingsProfileController, :index
      put "/", UserSettingsProfileController, :update
      get "/confirm_email/:token", UserSettingsController, :confirm_email

      resources "/ssh", UserSettingsSshController
    end

    # log out, confirm
    scope "/", EGitWeb do
      pipe_through [:browser]

      delete "/log_out", UserSessionController, :delete
      get "/confirm", UserConfirmationController, :new
      post "/confirm", UserConfirmationController, :create
      get "/confirm/:token", UserConfirmationController, :edit
      post "/confirm/:token", UserConfirmationController, :update
    end
  end
end
