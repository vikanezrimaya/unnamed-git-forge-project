import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :egit, EGit.Repo, database: "./test.db"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :egit_web, EGitWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "4OMSRaIuGN60aR/poZlGMrGGXMwKNibjcqilZRyhrhvSz1Dl2fb/IuMIQxEvaidm",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# In test we don't send emails.
config :egit, EGit.Mailer, adapter: Swoosh.Adapters.Test

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
